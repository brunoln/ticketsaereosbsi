package br.pucpr.bsi.prog3.ticketsAereosBSI.view.list;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyVetoException;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import br.pucpr.bsi.prog3.ticketsAereosBSI.bc.AeroportoBC;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Aeroporto;
import br.pucpr.bsi.prog3.ticketsAereosBSI.view.AeroportoView;
import br.pucpr.bsi.prog3.ticketsAereosBSI.view.GridBagLayoutUtility;
import br.pucpr.bsi.prog3.ticketsAereosBSI.view.TelaPrincipal;
import br.pucpr.bsi.prog3.ticketsAereosBSI.view.table.AeroportoTable;

/**
 * Apresenta uma lista com os aeroportos cadastrados. 
 * 
 * <p>A partir dessa tela eh possivel criar e editar Aeroportos.</p>
 * 
 * @author Bruno Nascimento, Kelvin Rivera
 */
public class ListaDeAeroportos extends JInternalFrame {
	private static final long serialVersionUID = 1L;
	
	private AeroportoTable tabela;
	private JScrollPane scrollPane;
	private JButton bNovoAeroporto;
	private JButton bAtualizaLista;
	private AeroportoView incluirFrame;
	private TelaPrincipal tela;
	
	public ListaDeAeroportos(TelaPrincipal tela) {
		super("Lista de Aeroportos", true, true, true, true);
		this.tela = tela;
		inicializaComponentes();
		adicionaComponentes();
		pack();
		//setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
		
		SwingUtilities.invokeLater(newAtualizaAeroportoAction());
	}

	private void inicializaComponentes() {
		tabela = new AeroportoTable();
		tabela.addMouseListener(new EditarAeroportoListener());
		scrollPane = new JScrollPane();
		scrollPane.setViewportView(tabela);

		bNovoAeroporto = new JButton();
		bNovoAeroporto.setText("Nova");
		bNovoAeroporto.setMnemonic(KeyEvent.VK_N);
		bNovoAeroporto.addActionListener(new NovoAeroportoListener());
		
		bAtualizaLista = new JButton();
		bAtualizaLista.setText("Atualizar");
		bAtualizaLista.setMnemonic(KeyEvent.VK_A);
		bAtualizaLista.addActionListener(new AtualizarListaListener());		
		
		incluirFrame = new AeroportoView(this);
	}
	
	private void adicionaComponentes(){
		add(scrollPane);
		JPanel panel = new JPanel();
		panel.add(bNovoAeroporto);
		panel.add(bAtualizaLista);
		add(panel, BorderLayout.SOUTH);
	}
	
	public Runnable newAtualizaAeroportoAction() {
		return new Runnable() {
			public void run() {
					List<Aeroporto> aeroportos = null;
					
					AeroportoBC bc = AeroportoBC.getInstance();
					aeroportos = bc.getList();
					tabela.reload(aeroportos);
			}
		};
	}
	
	public void refreshTable(List<Aeroporto> aeroporto) {
		tabela.reload(aeroporto);
	}
	
	private class AtualizarListaListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			SwingUtilities.invokeLater(newAtualizaAeroportoAction());
		}
	}
	
	private class NovoAeroportoListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			criatela("Aeroporto", incluirFrame);
		}
	}
	
	public void criatela(String titulo, GridBagLayoutUtility comp) {
		comp.setTitle(titulo);
		int pointX = (tela.dpArea.getWidth() - comp.getWidth()) / 2;  
		int pointY = (tela.dpArea.getHeight() - comp.getHeight()) / 2;  
		comp.setLocation(pointX, pointY);
		comp.show();
		tela.dpArea.remove(comp);
		tela.dpArea.add(comp);
		comp.toFront();
		if (!comp.isSelected()) {
			try {
				comp.setSelected(true);
			} catch (PropertyVetoException e) {
				e.printStackTrace();
			}
		}
	}
	
	private class EditarAeroportoListener extends MouseAdapter {
		public void mouseClicked(MouseEvent event) {
			if (event.getClickCount() == 2) {
				Aeroporto m = tabela.getAeroportoSelected();
				if (m != null) {
					//editarFrame.setVisible(true);
					//Chama para edição
				}
			}
		}
	}
}