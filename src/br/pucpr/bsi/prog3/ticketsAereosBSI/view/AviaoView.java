package br.pucpr.bsi.prog3.ticketsAereosBSI.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import br.pucpr.bsi.prog3.ticketsAereosBSI.bc.AviaoBC;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Aviao;

public class AviaoView extends GridBagLayoutUtility {

	private static final long serialVersionUID = 1L;
	private JTextField txtCodigo = new JTextField();
	private JTextField txtCargaTotal = new JTextField();

	public AviaoView() {
		this("Cadastro de avião", 400, 200);
	}

	public AviaoView(String title, int x, int y) {
		super(title, x, y);
		createScreen();
		
		okButton.addActionListener(new SalvarAviaoListener());
	}

	private void createScreen() {
		adiciona("Código", txtCodigo);
		adiciona("Carga Total", txtCargaTotal);

		adicionaBotaoSalvarECancelar();
	}

	private void resetForm() {	
		txtCodigo.setText("");
		txtCargaTotal.setText("");
	}
	
	private class SalvarAviaoListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				Aviao m = new Aviao(txtCodigo.getText(), Double.parseDouble(txtCargaTotal.getText()), null);
				AviaoBC bc = AviaoBC.getInstance();
				bc.create(m);
				setVisible(false);
				resetForm();
			} catch(Exception ex) {
				JOptionPane.showMessageDialog(AviaoView.this, 
						ex.getMessage(), "Erro ao salvar Avião", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
}