package br.pucpr.bsi.prog3.ticketsAereosBSI.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import br.pucpr.bsi.prog3.ticketsAereosBSI.bc.CiaAereaBC;
import br.pucpr.bsi.prog3.ticketsAereosBSI.bc.PapelBC;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.CiaAerea;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Papel;

public class PapelView extends GridBagLayoutUtility {

	private static final long serialVersionUID = 1L;
	private JComboBox<CiaAerea> cbCiaAerea = new JComboBox<CiaAerea>();
	private JTextField txtNome = new JTextField();
	private JTextField txtDescricao = new JTextField();

	public PapelView() {
		this("Cadastro de papel", 400, 200);
	}

	public PapelView(String title, int x, int y) {
		super(title, x, y);
		createScreen();

		okButton.addActionListener(new SalvarPapelListener());
	}

	private void createScreen() {
		carregaComboBox();

		adicionaComboLinhaUnica("Cia Aérea:", cbCiaAerea);
		adiciona("Nome:", txtNome);
		adiciona("Descrição:", txtDescricao);
		adicionaBotaoSalvarECancelar();
	}

	private void carregaComboBox() {
		cbCiaAerea.removeAllItems();
		List<CiaAerea> cias = CiaAereaBC.getInstance().getList();
		for (CiaAerea cia : cias) {
			cbCiaAerea.addItem(cia);
		}
	}

	private void resetForm() {	
		txtNome.setText("");
		txtDescricao.setText("");
	}

	private String validador() {
		StringBuilder sb = new StringBuilder();
		sb.append(txtNome.getText() == null || "".equals(txtNome.getText().trim()) ? "Nome, " : "");
		sb.append(txtDescricao.getText() == null || "".equals(txtDescricao.getText().trim()) ? "Descrição " : "");

		if (!sb.toString().isEmpty()) {
			sb.delete(sb.toString().length()-2, sb.toString().length());
		}
		return sb.toString();
	}
	
	protected Papel loadPapelFromPanel() {

		String msg = validador();
		if (!msg.isEmpty()) {
			throw new RuntimeException("Informe o(s) campo(s): "+msg);
		}

		String nome = txtNome.getText().trim();
		String descricao = txtDescricao.getText().trim();

		if (cbCiaAerea.getSelectedItem() == null) {
			throw new RuntimeException("Cadastre uma Cia Aérea!");
		}
		
		System.out.println(cbCiaAerea.getSelectedItem());
		return new Papel(nome, descricao, (CiaAerea) cbCiaAerea.getSelectedItem());
	}
	
	private class SalvarPapelListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				Papel m = loadPapelFromPanel();
				PapelBC bc = PapelBC.getInstance();
				bc.create(m);
				setVisible(false);
				resetForm();
			} catch(Exception ex) {
				JOptionPane.showMessageDialog(PapelView.this, 
						ex.getMessage(), "Erro ao salvar Papel", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
}