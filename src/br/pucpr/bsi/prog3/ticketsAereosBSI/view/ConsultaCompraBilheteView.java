package br.pucpr.bsi.prog3.ticketsAereosBSI.view;

public class ConsultaCompraBilheteView extends GridBagLayoutUtility {

	private static final long serialVersionUID = 1L;

	public ConsultaCompraBilheteView() {
		this("Consulta e reserva de Bilhetes", 400, 300);
	}
	
	public ConsultaCompraBilheteView(String title, int x, int y) {
		super(title, x, y);
	}
}
