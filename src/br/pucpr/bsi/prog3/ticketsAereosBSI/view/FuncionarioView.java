package br.pucpr.bsi.prog3.ticketsAereosBSI.view;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import br.pucpr.bsi.prog3.ticketsAereosBSI.bc.CiaAereaBC;
import br.pucpr.bsi.prog3.ticketsAereosBSI.bc.FuncionarioBC;
import br.pucpr.bsi.prog3.ticketsAereosBSI.bc.PapelBC;
import br.pucpr.bsi.prog3.ticketsAereosBSI.inter.CriaEndereco;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.CiaAerea;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Funcionario;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Papel;
import br.pucpr.bsi.prog3.ticketsAereosBSI.util.Funcoes;

public class FuncionarioView extends GridBagLayoutUtility implements CriaEndereco {
	private static final long serialVersionUID = 1L;
	private JComboBox<CiaAerea> cbCiaAerea = new JComboBox<CiaAerea>();
	private JComboBox<Papel> cbPapel = new JComboBox<Papel>();
	private JTextField txtNome = new JTextField();
	private JTextField txtEmail = new JTextField();
	private JTextField txtTelefone = new JTextField();
	private JFormattedTextField txtDataNasc = new JFormattedTextField(Funcoes.setMascara("##/##/####"));
	private JTextField txtCodigo = new JTextField();
	private JTextField txtContaCorrente = new JTextField();
	private JTextField txtEndereco = new JTextField();
	private JButton btEndereco = new JButton("Endereço");
	private EnderecoView enderecoView;
	private Endereco endereco;

	public FuncionarioView() {
		this("Cadastro de Funcionario", 400, 480);
	}
	
	public FuncionarioView(String title, int x, int y) {
		super(title, x, y);
		//this.setLocationRelativeTo( null );
		createScreen();
		enderecoView = new EnderecoView(this);
	}

	private void createScreen() {
		montaComboBoxCia();
		montaComboBoxPapel();
		
		adicionaComboLinhaUnica("Cia Aerea:", cbCiaAerea);
		adicionaComboLinhaUnica("Papel:", cbPapel);
		adiciona("Nome:", txtNome);
		adiciona("E-mail:", txtEmail);
		adiciona("Telefone:", txtTelefone);
		adiciona("Data Nascimento:", txtDataNasc);
		adiciona("Código:", txtCodigo);
		adiciona("Conta Corrente:", txtContaCorrente);
		adiciona("Endereço:", txtEndereco);
		txtEndereco.setEnabled(false);
		
		adicionaBotaoSalvarCancelarEndereco(btEndereco);
		
		cbCiaAerea.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				montaComboBoxPapel();
			}
		});
		
		okButton.addActionListener(new SalvarFuncionarioListener());
		btEndereco.addActionListener(new AbreEnderecoListener());
	}
	
	private void montaComboBoxCia() {
		cbCiaAerea.removeAllItems();
		List<CiaAerea> cias = CiaAereaBC.getInstance().getList();
		for (CiaAerea cia : cias) {
			cbCiaAerea.addItem(cia);
		}
	}
	
	private void montaComboBoxPapel() {
		cbPapel.removeAllItems();
		PapelBC bc = PapelBC.getInstance();
		for (Papel cia : bc.getList((CiaAerea) cbCiaAerea.getSelectedItem())) {
			cbPapel.addItem(cia);
		}
	}

	private void resetForm() {
		montaComboBoxCia();
		montaComboBoxPapel();
		txtNome.setText("");
		txtEmail.setText("");
		txtTelefone.setText("");
		txtDataNasc.setText("");
		txtCodigo.setText("");
		txtContaCorrente.setText("");
		txtEndereco.setText("");
	}
	
	private String validador() {
		
		StringBuilder sb = new StringBuilder();
		sb.append(txtNome.getText() == null || "".equals(txtNome.getText().trim()) ? "Nome, " : "");
		sb.append(txtCodigo.getText() == null || "".equals(txtCodigo.getText().trim()) ? "Código, " : "");
		sb.append(txtEmail.getText() == null || "".equals(txtEmail.getText().trim()) ? "Email, " : "");
		sb.append(txtTelefone.getText() == null || "".equals(txtTelefone.getText().trim()) ? "Telefone, " : "");
		sb.append(txtContaCorrente.getText() == null || "".equals(txtContaCorrente.getText().trim()) ? "ContaCorrente, " : "");
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy");
		sb.append(txtDataNasc.getText() == null || "".equals(txtDataNasc.getText().trim()) ? "Data de nascimento, " : 
			(txtDataNasc.getText().trim().length() != dateFormat.toPattern().length()) ? "Informe uma data no formato: dd/mm/yyyy, " : "");
		sb.append(txtEndereco.getText() == null || "".equals(txtEndereco.getText().trim()) ? "Endereço, " : "");
		if (!sb.toString().isEmpty()) {
			sb.delete(sb.toString().length()-2, sb.toString().length());
		}
		return sb.toString();
	}
	
	@SuppressWarnings("unused")
	private Funcionario loadFuncionarioFromPanel() {
		
		String msg = validador();
		if (!msg.isEmpty()) {
			throw new RuntimeException("Informe o(s) campo(s): "+msg);
		}
		
		String nome = txtNome.getText().trim();
		String email = txtEmail.getText().trim();
		String telefone = txtTelefone.getText().trim();
		String codigo = txtCodigo.getText().trim();
		String contaCorrente = txtContaCorrente.getText().trim();
		String endereco = txtEndereco.getText().trim();
		//Necessário realizar validação no formato da data antes = dd/mm/YYYY.
	
		Date dataNasc = Funcoes.strDateToDate(txtDataNasc.getText().trim());
		
		System.out.println(dataNasc);
		
		if (cbCiaAerea.getSelectedItem() == null) {
			throw new RuntimeException("Cadastre uma Cia Aérea!");
		} else if (cbPapel.getSelectedItem() == null) {
			throw new RuntimeException("Cadastre um Papel!");
		}
		
		Funcionario funcionario = new Funcionario(nome, email, telefone, dataNasc, this.endereco); 
		funcionario.setPapel((Papel) cbPapel.getSelectedItem());
		
		return funcionario;
	}
	
	private class SalvarFuncionarioListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				Funcionario m = loadFuncionarioFromPanel();
				FuncionarioBC bc = FuncionarioBC.getInstance();
				bc.create(m);
				
				setVisible(false);
				resetForm();
				btEndereco.setEnabled(true);

			} catch(Exception ex) {
				JOptionPane.showMessageDialog(FuncionarioView.this, 
						ex.getMessage(), "Erro ao salvar Funcionário", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	private class AbreEnderecoListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			criaTelaInterna("Endereço", enderecoView);
		}
	}
		
	public void desabilitaBotaoEndereco() {
		btEndereco.setEnabled(false);
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	@Override
	public void incluiRuaEndereco(String text) {
		txtEndereco.setText(text);		
	}
}
