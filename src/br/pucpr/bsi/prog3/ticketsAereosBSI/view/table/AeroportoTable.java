package br.pucpr.bsi.prog3.ticketsAereosBSI.view.table;

import java.util.List;

import javax.swing.JTable;

import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Aeroporto;

/**
 * <code>JTable</code> com operações customizadas para entidade <code>Aeroporto</code>.
 * 
 * 
 * @author Bruno Nascimento
 */
public class AeroportoTable extends JTable {

	private static final long serialVersionUID = 1L;

	private AeroportoTableModel modelo;
	
	public AeroportoTable() {
		modelo = new AeroportoTableModel();
		setModel(modelo);
	}
	
	public Aeroporto getAeroportoSelected() {
		int i = getSelectedRow();
		if (i < 0) {
			return null;
		}
		return modelo.getAeroportoAt(i);
	}
	
	public void reload(List<Aeroporto> aeroporto) {
		modelo.reload(aeroporto);
	}
}