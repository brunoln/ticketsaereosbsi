package br.pucpr.bsi.prog3.ticketsAereosBSI.view.table;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Aeroporto;

/**
 * Define um TableModel para entidade <code>Aeroporto</code>, considerando as colunas:
 * <ul>
 *   <li>Nome;</li>
 *   <li>Código;</li>
 *   <li>Endereço;</li>
 * </ul> 
 * 
 * @author Bruno Nascimento
 */


public class AeroportoTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	
	private List<Aeroporto> aeroportos;
	
	private String[] colNomes = { "Nome", "Código", "Endereço"};
	private Class<?>[] colTipos = { String.class, String.class, String.class};
	
	public AeroportoTableModel(){
	}
	
	public void reload(List<Aeroporto> aeroportos) {
		this.aeroportos = aeroportos;
		//atualiza o componente na tela
		fireTableDataChanged();
	}

	@Override
	public Class<?> getColumnClass(int coluna) {
		return colTipos[coluna];
	}

	@Override
	public int getColumnCount() {
		return 3;
	}

	@Override
	public String getColumnName(int coluna) {
		return colNomes[coluna];
	}

	@Override
	public int getRowCount() {
		if (aeroportos == null){
			return 0;
		}
		return aeroportos.size();
	}

	@Override
	public Object getValueAt(int linha, int coluna) {
		Aeroporto m = aeroportos.get(linha);
		switch (coluna) {
		case 0:
			return m.getNome();
		case 1:
			return m.getCodigo();
		case 2:
			return m.getEndereco().getRua();
		default:
			return null;
		}
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}
	
	public Aeroporto getAeroportoAt(int index) {
		return aeroportos.get(index);
	}
}