package br.pucpr.bsi.prog3.ticketsAereosBSI.view.table;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Aeroporto;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Aviao;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Voo;

/**
 * Define um TableModel para entidade <code>Aeroporto</code>, considerando as colunas:
 * <ul>
 *   <li>Nome;</li>
 *   <li>Código;</li>
 *   <li>Endereço;</li>
 * </ul> 
 * 
 * @author Bruno Nascimento
 */


public class VooTableModel extends AbstractTableModel {
	private static final long serialVersionUID = 1L;
	
	private List<Voo> voos;
	
	private String[] colNomes = { "Vôo", "Avião", "Partida", "Chegada", "Classe"};
	private Class<?>[] colTipos = { String.class, Aviao.class, Aeroporto.class, Aeroporto.class, String.class};
	
	public VooTableModel(){
	}
	
	public void reload(List<Voo> voos) {
		this.voos = voos;
		//atualiza o componente na tela
		fireTableDataChanged();
	}

	@Override
	public Class<?> getColumnClass(int coluna) {
		return colTipos[coluna];
	}

	@Override
	public int getColumnCount() {
		return 5;
	}

	@Override
	public String getColumnName(int coluna) {
		return colNomes[coluna];
	}

	@Override
	public int getRowCount() {
		if (voos == null){
			return 0;
		}
		return voos.size();
	}

	@Override
	public Object getValueAt(int linha, int coluna) {
		Voo m = voos.get(linha);
		switch (coluna) {
		case 0:
			return m.getNome();
		case 1:
			return m.getCieAerea();
		case 2:
			return m.getEmbarque();
		case 3:
			return m.getDesembarque();
		case 4:
			return "";
		default:
			return null;
		}
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}
	
	public Voo getVooAt(int index) {
		return voos.get(index);
	}
}