package br.pucpr.bsi.prog3.ticketsAereosBSI.view.table;

import java.util.List;

import javax.swing.JTable;

import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Voo;

/**
 * <code>JTable</code> com operações customizadas para entidade <code>Aeroporto</code>.
 * 
 * 
 * @author Bruno Nascimento
 */
public class VooTable extends JTable {
	private static final long serialVersionUID = 1L;
	
	private VooTableModel modelo;
	
	public VooTable() {
		modelo = new VooTableModel();
		setModel(modelo);
	}
	
	public Voo getVooSelected() {
		int i = getSelectedRow();
		if (i < 0) {
			return null;
		}
		return modelo.getVooAt(i);
	}
	
	public void reload(List<Voo> voos) {
		modelo.reload(voos);
	}
}