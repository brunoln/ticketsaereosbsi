package br.pucpr.bsi.prog3.ticketsAereosBSI.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import br.pucpr.bsi.prog3.ticketsAereosBSI.bc.AeroportoBC;
import br.pucpr.bsi.prog3.ticketsAereosBSI.inter.CriaEndereco;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Aeroporto;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsAereosBSI.view.list.ListaDeAeroportos;

public class AeroportoView extends GridBagLayoutUtility implements CriaEndereco {

	private static final long serialVersionUID = 1L;
	private JTextField txtNome = new JTextField();
	private JTextField txtCodigo = new JTextField();
	private JTextField txtEndereco = new JTextField();
	private JButton btEndereco = new JButton("Endereço");
	private EnderecoView enderecoView;
	private Endereco endereco;
	private ListaDeAeroportos framePrincipal;

	public AeroportoView(ListaDeAeroportos framePrincipal) {
		this("Cadastro de Aeroporto", 400, 250);
		this.framePrincipal = framePrincipal;
		//this.setLocationRelativeTo( null );
	}

	public AeroportoView(String title, int x, int y) {
		super(title, x, y);
		createScreen();

		okButton.addActionListener(new SalvarAeroportoListener());
		enderecoView = new EnderecoView(this);
	}

	private void createScreen() {
		adiciona("Nome", txtNome);
		adiciona("Código", txtCodigo);
		adiciona("Endereço", txtEndereco);

		adicionaBotaoSalvarCancelarEndereco(btEndereco);
		btEndereco.addActionListener(new AbreEnderecoListener());
		txtEndereco.setEnabled(false);
	}

	private void resetForm() {	
		txtNome.setText("");
		txtCodigo.setText("");
		txtEndereco.setText("");
	}

	private String validador() {
		StringBuilder sb = new StringBuilder();
		sb.append(txtNome.getText() == null || "".equals(txtNome.getText().trim()) ? "Nome, " : "");
		sb.append(txtCodigo.getText() == null || "".equals(txtCodigo.getText().trim()) ? "Código, " : "");
		sb.append(txtEndereco.getText() == null || "".equals(txtEndereco.getText().trim()) ? "Endereço, " : "");

		if (!sb.toString().isEmpty()) {
			sb.delete(sb.toString().length()-2, sb.toString().length());
		}
		return sb.toString();
	}

	public void incluiRuaEndereco(String text) {
		txtEndereco.setText(text);
	}


	protected Aeroporto loadAeroportoFromPanel() {

		String msg = validador();
		if (!msg.isEmpty()) {
			throw new RuntimeException("Informe o(s) campo(s): "+msg);
		}

		String nome = txtNome.getText().trim();
		String codigo = txtCodigo.getText().trim();

		if (nome.length() < 5) {
			throw new RuntimeException("O nome deve conter no minimo 5 caracteres!");
		}

		return new Aeroporto( nome, codigo, this.endereco);
	}

	private class SalvarAeroportoListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				Aeroporto m = loadAeroportoFromPanel();
				AeroportoBC bc = AeroportoBC.getInstance();
				bc.create(m);
				
				setVisible(false);
				resetForm();
				btEndereco.setEnabled(true);
				SwingUtilities.invokeLater(framePrincipal.newAtualizaAeroportoAction());

			} catch(Exception ex) {
				JOptionPane.showMessageDialog(AeroportoView.this, 
						ex.getMessage(), "Erro ao salvar Aeroporto", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	

	private class AbreEnderecoListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			criaTelaInterna("Endereço", enderecoView);
		}
	}
	
	public void desabilitaBotaoEndereco() {
		btEndereco.setEnabled(false);
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
}