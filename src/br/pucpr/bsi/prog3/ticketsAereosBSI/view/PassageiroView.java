package br.pucpr.bsi.prog3.ticketsAereosBSI.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import br.pucpr.bsi.prog3.ticketsAereosBSI.bc.PassageiroBC;
import br.pucpr.bsi.prog3.ticketsAereosBSI.inter.CriaEndereco;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Passageiro;
import br.pucpr.bsi.prog3.ticketsAereosBSI.util.Funcoes;

public class PassageiroView extends GridBagLayoutUtility implements CriaEndereco{
	
	private static final long serialVersionUID = 1L;
	private JTextField txtNome = new JTextField();
	private JTextField txtEmail = new JTextField();
	private JTextField txtTelefone = new JTextField();
	private JFormattedTextField txtDataNascimento = new JFormattedTextField(Funcoes.setMascara("##/##/####"));
	private JTextField txtCartao = new JTextField();
	private JTextField txtDocumento = new JTextField();
	private JTextField txtEndereco = new JTextField();
	private JButton btEndereco = new JButton("Endereço");
	private EnderecoView enderecoView;
	private Endereco endereco;
	
	public PassageiroView() {
		this("Cadastro de Passageiro", 410, 420);
	}
	
	public PassageiroView(String title, int x, int y) {
		super(title, x, y);
		createScreen();
		
		okButton.addActionListener(new SalvarPassageiroListener());
		enderecoView = new EnderecoView(this);
	}

	private void createScreen() {
		adiciona("Nome", txtNome);
		adiciona("Email", txtEmail);
		adiciona("Telefone", txtTelefone);
		adiciona("Data de Nascimento", txtDataNascimento);
		adiciona("Cartão", txtCartao);
		adiciona("Documento", txtDocumento);
		adiciona("Endereço", txtEndereco);
		adicionaBotaoSalvarCancelarEndereco(btEndereco);
		btEndereco.addActionListener(new AbreEnderecoListener());
		txtEndereco.setEnabled(false);
	}
	
	private void resetForm() {	
		txtNome.setText("");
		txtEmail.setText("");
		txtTelefone.setText("");
		txtEndereco.setText("");
		txtDataNascimento.setText("");
		txtCartao.setText("");
		txtDocumento.setText("");
	}
	
	private String validador() {
		StringBuilder sb = new StringBuilder();
		sb.append(txtNome.getText() == null || "".equals(txtNome.getText().trim()) ? "Nome, " : "");
		sb.append(txtEmail.getText() == null || "".equals(txtEmail.getText().trim()) ? "Email, " : "");
		sb.append(txtTelefone.getText() == null || "".equals(txtTelefone.getText().trim()) ? "Telefone, " : "");
		sb.append(txtEndereco.getText() == null || "".equals(txtEndereco.getText().trim()) ? "Endereço, " : "");

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy");
		sb.append(txtDataNascimento.getText() == null || "".equals(txtDataNascimento.getText().trim()) ? "Data de Nascimento, " : 
			(txtDataNascimento.getText().trim().length() != dateFormat.toPattern().length()) ? "Informe uma data no formato: dd/mm/yyyy, " : "");
		sb.append(txtCartao.getText() == null || "".equals(txtCartao.getText().trim()) ? "Cartão, " : "");
		sb.append(txtDocumento.getText() == null || "".equals(txtDocumento.getText().trim()) ? "Documento, " : "");

		if (!sb.toString().isEmpty()) {
			sb.delete(sb.toString().length()-2, sb.toString().length());
		}
		return sb.toString();
	}

	public void incluiRuaEndereco(String text) {
		txtEndereco.setText(text);
	}
	
	protected Passageiro loadPassageiroFromPanel() {

		String msg = validador();
		if (!msg.isEmpty()) {
			throw new RuntimeException("Informe o(s) campo(s): "+msg);
		}

		String nome = txtNome.getText().trim();
		String email = txtEmail.getText().trim();
		String telefone = txtTelefone.getText().trim();

		//Necessário realizar validação no formato da data antes = dd/mm/YYYY.
		Date dataNasc = Funcoes.strDateToDate(txtDataNascimento.getText().trim());

		if (nome.length() < 5) {
			throw new RuntimeException("O nome deve conter no minimo 5 caracteres!");
		}

		return new Passageiro(nome, email, telefone, dataNasc, this.endereco);
	}
	
	private class SalvarPassageiroListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				Passageiro m = loadPassageiroFromPanel();
				PassageiroBC bc = PassageiroBC.getInstance();
				bc.create(m);
				
				setVisible(false);
				resetForm();
				btEndereco.setEnabled(true);
				
			} catch(Exception ex) {
				JOptionPane.showMessageDialog(PassageiroView.this, 
						ex.getMessage(), "Erro ao salvar Passageiro", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	private class AbreEnderecoListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			criaTelaInterna("Endereço", enderecoView);
		}
	}
	
	public void desabilitaBotaoEndereco() {
		btEndereco.setEnabled(false);
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
}
