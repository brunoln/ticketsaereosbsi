package br.pucpr.bsi.prog3.ticketsAereosBSI.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import br.pucpr.bsi.prog3.ticketsAereosBSI.bc.AviaoBC;
import br.pucpr.bsi.prog3.ticketsAereosBSI.bc.HorarioBC;
import br.pucpr.bsi.prog3.ticketsAereosBSI.bc.VooBC;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Aviao;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Horario;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Voo;
import br.pucpr.bsi.prog3.ticketsAereosBSI.util.Funcoes;

public class HorarioView extends GridBagLayoutUtility{
	
	private static final long serialVersionUID = 1L;
	private JComboBox<Aviao> cbAviao = new JComboBox<Aviao>();
	private JComboBox<Voo> cbVoo = new JComboBox<Voo>();
	private JTextField txtCodigo = new JTextField();
	private JFormattedTextField txtPartida = new JFormattedTextField(Funcoes.setMascara("##/##/####"));
	private JFormattedTextField txtChegada = new JFormattedTextField(Funcoes.setMascara("##/##/####"));
	private JTextField txtQtdEconomica = new JTextField();
	private JTextField txtQtdExecutiva = new JTextField();
	private JTextField txtQtdPrimeira = new JTextField();
	
	public HorarioView() {
		this("Cadastro de Horário", 400, 400);
	}
	
	public HorarioView(String title, int x, int y) {
		super(title, x, y);
		//this.setLocationRelativeTo( null );
		createScreen();
	}
	
	private void createScreen() {
		montaComboBoxAviao();
		montaComboBoxVoo();
		
		add("Avião", cbAviao);
		add("Voo", cbVoo);
		add("Código:", txtCodigo);
		add("Partida:", txtPartida);
		add("Chegada:", txtChegada);
		add("Qtd Econômica:", txtQtdEconomica);
		add("Qtd Executiva:", txtQtdExecutiva);
		add("Qtd Primeira:", txtQtdPrimeira);
		
		addButtonOKandClose2(this);
		okButton.addActionListener(new SalvarHorarioListener());
	}
	
	private void montaComboBoxAviao() {
		cbAviao.removeAllItems();
	
		List<Aviao> avioes = AviaoBC.getInstance().getList();
		for (Aviao aviao : avioes) {
			cbAviao.addItem(aviao);
		}
	}
	
	private void montaComboBoxVoo() {
		cbVoo.removeAllItems();
		ArrayList<Voo> voos = VooBC.getInstance().getList();
		for (Voo voo : voos) {
			cbVoo.addItem(voo);
		}
	}
	
	private void resetForm() {	
		montaComboBoxAviao();
		montaComboBoxVoo();
		txtCodigo.setText("");
		txtPartida.setText("");
		txtChegada.setText("");
		txtQtdEconomica.setText("");
		txtQtdExecutiva.setText("");
		txtQtdPrimeira.setText("");
	}
	
	private Horario loadHorarioFromPanel() {
		
		String codigo = txtCodigo.getText().trim();
		
		//Necessário realizar validação no formato da data antes = dd/mm/YYYY.
		Date partida = Funcoes.strDateToDate(txtPartida.getText().trim());
		Date chegada = Funcoes.strDateToDate(txtChegada.getText().trim());
		
		int economica = Integer.parseInt(txtQtdEconomica.getText().trim());
		int executiva = Integer.parseInt(txtQtdExecutiva.getText().trim());
		int primeira = Integer.parseInt(txtQtdPrimeira.getText().trim());
		
		if (cbAviao.getSelectedItem() == null) {
			throw new RuntimeException("Cadastre um Avião!");
		}
				
		if (cbVoo.getSelectedItem() == null) {
			throw new RuntimeException("Cadastre um Voo!");
		}
		
		return new Horario((Aviao) cbAviao.getSelectedItem(), (Voo) cbVoo.getSelectedItem(), codigo, 
				partida, chegada, economica, executiva, primeira);
	}
	
	private class SalvarHorarioListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				Horario horario = loadHorarioFromPanel();
				HorarioBC bc = HorarioBC.getInstance();
				bc.create(horario);
				
				setVisible(false);
				resetForm();
			} catch(Exception ex) {
				JOptionPane.showMessageDialog(HorarioView.this, 
						ex.getMessage(), "Erro ao salvar Horário", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
}
