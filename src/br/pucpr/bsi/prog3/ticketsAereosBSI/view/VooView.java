package br.pucpr.bsi.prog3.ticketsAereosBSI.view;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import br.pucpr.bsi.prog3.ticketsAereosBSI.bc.AeroportoBC;
import br.pucpr.bsi.prog3.ticketsAereosBSI.bc.CiaAereaBC;
import br.pucpr.bsi.prog3.ticketsAereosBSI.bc.VooBC;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Aeroporto;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.CiaAerea;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Voo;

public class VooView extends GridBagLayoutUtility {
	private static final long serialVersionUID = 1L;
	private JComboBox<CiaAerea> cbCiaAerea = new JComboBox<CiaAerea>();
	private JComboBox<Aeroporto> cbEmbarque = new JComboBox<Aeroporto>();
	private JComboBox<Aeroporto> cbDesembarque = new JComboBox<Aeroporto>();
	private JTextField txtNome= new JTextField();
	private JTextField txtDescricao = new JTextField();

	public VooView() {
		this("Cadastro de Funcionario", 400, 250);
	}
	
	public VooView(String title, int x, int y) {
		super(title, x, y);
		//this.setLocationRelativeTo( null );
		createScreen();
	}

	private void createScreen() {
		montaComboBoxCia();
		montaComboBoxEmbarque();
		montaComboBoxDesembarque();
		
		add("Cia Aerea:", cbCiaAerea);
		add("Embarque:", cbEmbarque);
		add("Desembarque:", cbDesembarque);
		add("Nome:", txtNome);
		add("Descricao:", txtDescricao);
		
		addButtonOKandClose2(this);
		
		/*cbEmbarque.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cbEmbarque.removeAllItems();
				ArrayList<Aeroporto> aeroportos = AeroportoBC.getInstance().getList();
				aeroportos.remove((Aeroporto) cbDesembarque.getSelectedItem());
				for (Aeroporto aeroporto : aeroportos) {
					cbEmbarque.addItem(aeroporto);
				}
			}
		});
		
		cbDesembarque.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cbDesembarque.removeAllItems();
				ArrayList<Aeroporto> aeroportos = AeroportoBC.getInstance().getList();
				aeroportos.remove((Aeroporto) cbEmbarque.getSelectedItem());
				for (Aeroporto aeroporto : aeroportos) {
					cbDesembarque.addItem(aeroporto);
				}
			}
		});*/
		
		okButton.addActionListener(new SalvarVooListener());
	}
	
	private void montaComboBoxCia() {
		cbCiaAerea.removeAllItems();
		List<CiaAerea> cias = CiaAereaBC.getInstance().getList();
		for (CiaAerea cia : cias) {
			cbCiaAerea.addItem(cia);
		}
	}
	
	private void montaComboBoxEmbarque() {
		cbEmbarque.removeAllItems();
		List<Aeroporto> aeroportos = AeroportoBC.getInstance().getList();
		for (Aeroporto aeroporto : aeroportos) {
			cbEmbarque.addItem(aeroporto);
		}
	}
	
	private void montaComboBoxDesembarque() {
		cbDesembarque.removeAllItems();
		List<Aeroporto> aeroportos = AeroportoBC.getInstance().getList();
		for (Aeroporto aeroporto : aeroportos) {
			cbDesembarque.addItem(aeroporto);
		}
	}

	private void resetForm() {	
		montaComboBoxCia();
		montaComboBoxEmbarque();
		montaComboBoxDesembarque();
		txtNome.setText("");
		txtDescricao.setText("");
	}
	
	private String validador() {
		StringBuilder sb = new StringBuilder();
		sb.append(txtNome.getText() == null || "".equals(txtNome.getText().trim()) ? "Nome, " : "");
		sb.append(txtDescricao.getText() == null || "".equals(txtDescricao.getText().trim()) ? "Endereço, " : "");
		if (!sb.toString().isEmpty()) {
			sb.delete(sb.toString().length()-2, sb.toString().length());
		}
		return sb.toString();
	}
	
	private Voo loadVooFromPanel() {
		
		String msg = validador();
		if (!msg.isEmpty()) {
			throw new RuntimeException("Informe o(s) campo(s): "+msg);
		}
		
		String nome = txtNome.getText().trim();
		String descricao = txtDescricao.getText().trim();
		
		if (cbCiaAerea.getSelectedItem() == null) {
			throw new RuntimeException("Cadastre uma Cia Aérea");
		}
				
		if (cbEmbarque.getSelectedItem() == null) {
			throw new RuntimeException("Cadastre um Aeroporto para embarque!");
		} 
		
		if (cbDesembarque.getSelectedItem() == null) {
			throw new RuntimeException("Cadastre um Aeroporto para desembarque!");
		}
		
		if (cbDesembarque.getSelectedItem() == cbEmbarque.getSelectedItem()) {
			throw new RuntimeException("O embarque e o desembarque não podem ser iguais!");
		}
		
		return new Voo(nome, descricao, (CiaAerea) cbCiaAerea.getSelectedItem(), (Aeroporto) cbEmbarque.getSelectedItem(), 
				(Aeroporto) cbDesembarque.getSelectedItem());
	}
	
	private class SalvarVooListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				Voo voo = loadVooFromPanel();
				VooBC bc = VooBC.getInstance();
				bc.create(voo);
				
				setVisible(false);
				resetForm();
				
			} catch(Exception ex) {
				JOptionPane.showMessageDialog(VooView.this, 
						ex.getMessage(), "Erro ao salvar Voo", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
}
