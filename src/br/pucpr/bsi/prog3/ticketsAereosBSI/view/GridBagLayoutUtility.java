package br.pucpr.bsi.prog3.ticketsAereosBSI.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class GridBagLayoutUtility extends javax.swing.JInternalFrame {

	private static final long serialVersionUID = 3720039751606208508L;
	
	protected JButton okButton = new JButton("Salvar");
	protected JButton closeButton = new JButton("Cancelar");
	protected JPanel jpanel;
	private int row = 0;
	
	public GridBagLayoutUtility(String title, int x, int y ) {  
		super(title, false, true, false, true);
		jpanel = new JPanel();
		this.setContentPane(jpanel);
		this.getContentPane().setLayout(new GridBagLayout());  
		this.setSize(x, y);
		
		closeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
	}  

	public void addButtonOKandClose(JInternalFrame frame) {
		Box buttonBox = Box.createHorizontalBox();
	    buttonBox.add(okButton);
	    buttonBox.add(Box.createHorizontalStrut(20));
	    buttonBox.add(closeButton);
	    add(frame, buttonBox, 2, 5, 1, 1, GridBagConstraints.NORTHEAST);
	}
	
	public void addButtonOKandClose2(JInternalFrame frame) {
		Box buttonBox = Box.createHorizontalBox();
	    buttonBox.add(okButton);
	    buttonBox.add(Box.createHorizontalStrut(20));
	    buttonBox.add(closeButton);
	    add(frame, buttonBox, 5, 8, 1, 1, GridBagConstraints.NORTHEAST);
	}
	
	public void addButtonOKAndCloseAndAnother(JInternalFrame frame, JButton button, int row) {
		Box buttonBox = Box.createHorizontalBox();
		buttonBox.add(button);
	    buttonBox.add(Box.createHorizontalStrut(20));
	    buttonBox.add(okButton);
	    buttonBox.add(Box.createHorizontalStrut(20));
	    buttonBox.add(closeButton);
	    add(frame, buttonBox, 2, row, 1, 1, GridBagConstraints.NORTHEAST);
	}
	
	/** 
	 * Adiciona um label e um componente horizontalmente 
	 * @param label String que irá aparecer no label 
	 * @param componente Componente de edição 
	 */  
	public void add(String label, JComponent componente ) {  
		GridBagConstraints cons = new GridBagConstraints();  
		cons.fill = GridBagConstraints.NONE;  
		cons.anchor = GridBagConstraints.NORTHWEST;  
		cons.insets = new Insets(5, 5, 5, 5);  

		cons.weightx = 0;  
		cons.gridwidth = 1;  
		this.getContentPane().add(new JLabel(label), cons);  

		cons.fill = GridBagConstraints.BOTH;  
		cons.weightx = 1;  
		cons.gridwidth = GridBagConstraints.REMAINDER;  
		this.getContentPane().add(componente, cons);  
	}
	
	public void addTable(JComponent componente) {
		GridBagConstraints cons = new GridBagConstraints();
		/*cons.fill = GridBagConstraints.HORIZONTAL;  
		cons.anchor = GridBagConstraints.NORTHWEST;  */
		cons.anchor = GridBagConstraints.CENTER;
		cons.gridy = row;
		cons.ipady = 80;
		cons.fill = GridBagConstraints.BOTH; 
		cons.weightx = 1.0;
//		cons.weighty = 3.0;
		cons.gridwidth = GridBagConstraints.REMAINDER;
		cons.gridheight = 3;
        this.getContentPane().add(componente, cons);
        
        row += 3;
	}
	
	public void addTableCompra(JComponent componente) {
		GridBagConstraints cons = new GridBagConstraints();
		/*cons.fill = GridBagConstraints.HORIZONTAL;  
		cons.anchor = GridBagConstraints.NORTHWEST;  */
		cons.anchor = GridBagConstraints.CENTER;
		cons.gridy = row;
		cons.ipady = 80;
		cons.fill = GridBagConstraints.BOTH; 
		cons.weightx = 1.0;
//		cons.weighty = 3.0;
		cons.gridwidth = 5;
		cons.gridheight = 3;
        this.getContentPane().add(componente, cons);
        
        row += 3;
	}
	
	/** 
	 * Adiciona um label e um componente horizontalmente. O componente ocupará todo o reto da tela 
	 * @param label String que irá aparecer no label 
	 * @param componente Componente de edição 
	 */  
	public void add(String label, JScrollPane componente ) {  
		GridBagConstraints cons = new GridBagConstraints();  
		cons.fill = GridBagConstraints.NONE;  
		cons.anchor = GridBagConstraints.NORTHWEST;  
		cons.insets = new Insets(5, 5, 5, 5);  
		cons.weighty = 1;  
		cons.gridheight = GridBagConstraints.REMAINDER;  

		cons.weightx = 0;  
		cons.gridwidth = 1;  
		this.getContentPane().add(new JLabel(label), cons);  

		cons.fill = GridBagConstraints.BOTH;  
		cons.weightx = 1;  
		cons.gridwidth = GridBagConstraints.REMAINDER;  
		this.getContentPane().add(componente, cons);  
	}
	
	/** 
	 * Adiciona um label, um componente de edição, mais um label e outro componente de edição. Todos  
	 * na mesma linha 
	 * @param label Label 1  
	 * @param componente Componente de edição 
	 * @param label2 Label 2 
	 * @param componente2 Componente de edição 2 
	 */  
	public void add(String label, JComponent componente, String label2, JComponent componente2) {  
		GridBagConstraints cons = new GridBagConstraints();  
		cons.fill = GridBagConstraints.BOTH;  
		cons.insets = new Insets(5, 5, 5, 5);  

		cons.fill = GridBagConstraints.NONE;  
		cons.anchor = GridBagConstraints.NORTHWEST;  
		cons.weightx = 0;  
		cons.gridwidth = 1;  
		this.getContentPane().add(new JLabel(label), cons);  

		cons.weightx = 1;  
		cons.gridwidth = 1;  
		cons.fill = GridBagConstraints.BOTH;  
		this.getContentPane().add(componente, cons);  

		cons.fill = GridBagConstraints.NONE;  
		cons.weightx = 0;  
		cons.gridwidth = 1;  
		this.getContentPane().add(new JLabel(label2), cons);  

		cons.weightx = 1;  
		cons.fill = GridBagConstraints.BOTH;  
		cons.gridwidth = GridBagConstraints.REMAINDER;  
		this.getContentPane().add(componente2, cons);  
	}      
	
	public void add(JInternalFrame p, JComponent c, int x, int y, int width, int height, int align) {
		GridBagConstraints gc = new GridBagConstraints();
		gc.gridx = x;
		gc.gridy = y;
		gc.gridwidth = width;
		gc.gridheight = height;
		gc.weightx = 100.0;
		gc.weighty = 100.0;
		gc.insets = new Insets(5, 5, 5, 5);
		gc.anchor = align;
		gc.fill = GridBagConstraints.NONE;
		p.add(c, gc);
	}
	
	
	public void adicionaBotaoSalvarECancelar() {
		GridBagConstraints gbBotaoSalvar = new GridBagConstraints();
		gbBotaoSalvar.insets = new Insets(2, 4, 2, 4);
		gbBotaoSalvar.fill = GridBagConstraints.NONE;
		gbBotaoSalvar.anchor = GridBagConstraints.CENTER;
		gbBotaoSalvar.gridx = 2;
		gbBotaoSalvar.gridy = row;
		gbBotaoSalvar.gridwidth = 1;
		gbBotaoSalvar.weightx = 1.0;
		this.getContentPane().add(okButton, gbBotaoSalvar);

		GridBagConstraints gbBotaoCancelar = new GridBagConstraints();
		gbBotaoCancelar.anchor = GridBagConstraints.CENTER;
		gbBotaoCancelar.insets = new Insets(2, 4, 2, 4);
		gbBotaoCancelar.gridx = 3;
		gbBotaoCancelar.gridy = row;
		gbBotaoCancelar.gridwidth = 1;
		gbBotaoSalvar.weightx = 1.0;
		gbBotaoCancelar.fill = GridBagConstraints.BOTH;
		this.getContentPane().add(closeButton, gbBotaoCancelar);

		row++;
	}
	
	public void adicionaBotaoCompra(JButton button) {
		GridBagConstraints gbBotao = new GridBagConstraints();
		gbBotao.insets = new Insets(2, 4, 2, 4);
		gbBotao.fill = GridBagConstraints.NONE;
		gbBotao.anchor = GridBagConstraints.CENTER;
		gbBotao.gridx = 3;
		gbBotao.gridy = row;
		gbBotao.gridwidth = 1;
		gbBotao.weightx = 1.0;
		this.getContentPane().add(button, gbBotao);
		row++;
	}
	
	public void adicionaBotaoSalvarCancelarEndereco(JButton button) {
		GridBagConstraints gbBotao = new GridBagConstraints();
		gbBotao.insets = new Insets(2, 4, 2, 4);
		gbBotao.fill = GridBagConstraints.NONE;
		gbBotao.anchor = GridBagConstraints.CENTER;
		gbBotao.gridx = 2;
		gbBotao.gridy = row;
		gbBotao.gridwidth = 1;
		gbBotao.weightx = 1.0;
		this.getContentPane().add(button, gbBotao);
		
		GridBagConstraints gbBotaoSalvar = new GridBagConstraints();
		gbBotaoSalvar.insets = new Insets(2, 4, 2, 4);
		gbBotaoSalvar.fill = GridBagConstraints.NONE;
		gbBotaoSalvar.anchor = GridBagConstraints.CENTER;
		gbBotaoSalvar.gridx = 3;
		gbBotaoSalvar.gridy = row;
		gbBotaoSalvar.gridwidth = 1;
		gbBotaoSalvar.weightx = 1.0;
		this.getContentPane().add(okButton, gbBotaoSalvar);

		GridBagConstraints gbBotaoCancelar = new GridBagConstraints();
		gbBotaoCancelar.anchor = GridBagConstraints.CENTER;
		gbBotaoCancelar.insets = new Insets(2, 4, 2, 4);
		gbBotaoCancelar.gridx = 4;
		gbBotaoCancelar.gridy = row;
		gbBotaoCancelar.gridwidth = 1;
		gbBotaoSalvar.weightx = 1.0;
		gbBotaoCancelar.fill = GridBagConstraints.BOTH;
		this.getContentPane().add(closeButton, gbBotaoCancelar);

		row++;
	}

	public void adicionaDoisCampos(String label, JComponent componente, String label2,
			JComponent componente2) {
		GridBagConstraints gbLabel1 = new GridBagConstraints();
		// gbLabel1.fill = GridBagConstraints.BOTH;
		gbLabel1.insets = new Insets(2, 4, 2, 4);
		gbLabel1.fill = GridBagConstraints.NONE;
		gbLabel1.anchor = GridBagConstraints.NORTHWEST;
		gbLabel1.gridx = 0;
		gbLabel1.gridy = row;
		gbLabel1.gridwidth = 2;
		gbLabel1.weightx = 1.0;
		this.getContentPane().add(new JLabel(label), gbLabel1);

		GridBagConstraints gbLabel2 = new GridBagConstraints();
		gbLabel2.insets = new Insets(2, 4, 2, 4);
		gbLabel2.anchor = GridBagConstraints.NORTHWEST;
		gbLabel2.fill = GridBagConstraints.NONE;
		gbLabel2.gridx = 3;
		gbLabel2.gridy = row;
		gbLabel2.gridwidth = 2;
		gbLabel2.weightx = 1.0;
		this.getContentPane().add(new JLabel(label2), gbLabel2);
		
		GridBagConstraints gbCampo1 = new GridBagConstraints();
		gbCampo1.insets = new Insets(2, 4, 2, 4);
		gbCampo1.gridx = 0;
		gbCampo1.gridy = ++row;
		gbCampo1.gridwidth = 3;
		gbCampo1.fill = GridBagConstraints.BOTH;
		gbCampo1.weightx = 1.0;
		this.getContentPane().add(componente, gbCampo1);

		GridBagConstraints gbCampo2 = new GridBagConstraints();
		gbCampo2.insets = new Insets(2, 4, 2, 4);
		gbCampo2.gridx = 3;
		gbCampo2.gridy = row;
		gbCampo2.fill = GridBagConstraints.BOTH;
		gbCampo2.gridwidth = 2;
		gbCampo2.weightx = 1.0;
		this.getContentPane().add(componente2, gbCampo2);

		row++;
		
	}

	public void adicionaDoisCamposUltimoMenor(String label, JComponent componente,
			String label2, JComponent componente2) {
		GridBagConstraints gbLabel1 = new GridBagConstraints();
		// gbLabel1.fill = GridBagConstraints.BOTH;
		gbLabel1.insets = new Insets(2, 4, 2, 4);
		gbLabel1.fill = GridBagConstraints.NONE;
		gbLabel1.anchor = GridBagConstraints.NORTHWEST;
		gbLabel1.gridx = 0;
		gbLabel1.gridy = row;
		gbLabel1.gridwidth = 3;
		gbLabel1.weightx = 1.0;
		this.getContentPane().add(new JLabel(label), gbLabel1);

		GridBagConstraints gbCampo1 = new GridBagConstraints();
		gbCampo1.insets = new Insets(2, 4, 2, 4);
		gbCampo1.gridx = 0;
		gbCampo1.gridy = ++row;
		gbCampo1.gridwidth = GridBagConstraints.RELATIVE;
		gbCampo1.fill = GridBagConstraints.BOTH;
		gbCampo1.weightx = 1.0;
		this.getContentPane().add(componente, gbCampo1);

		GridBagConstraints gbLabel2 = new GridBagConstraints();
		gbLabel2.insets = new Insets(2, 4, 2, 4);
		gbLabel2.anchor = GridBagConstraints.NORTHWEST;
		gbLabel2.fill = GridBagConstraints.NONE;
		gbLabel2.gridx = 4;
		gbLabel2.gridy = --row;
		gbLabel2.gridwidth = 1;
		gbLabel2.weightx = 1.0;
		this.getContentPane().add(new JLabel(label2), gbLabel2);

		GridBagConstraints gbCampo2 = new GridBagConstraints();
		gbCampo2.insets = new Insets(2, 4, 2, 4);
		gbCampo2.gridx = 4;
		gbCampo2.gridy = ++row;
		gbCampo2.fill = GridBagConstraints.BOTH;
		gbCampo2.gridwidth = 1;
		gbCampo2.weightx = 1.0;
		this.getContentPane().add(componente2, gbCampo2);
		row++;
	}

	public void adiciona(String label, JTextField txt) {
		GridBagConstraints gbLabel = new GridBagConstraints();
		gbLabel.fill = GridBagConstraints.NONE;
		gbLabel.anchor = GridBagConstraints.NORTHWEST;
		gbLabel.insets = new Insets(2, 4, 2, 4);
		gbLabel.gridx = 0;
		gbLabel.gridy = row;
		gbLabel.weightx = 1.0;
		gbLabel.gridwidth = 4;
		this.getContentPane().add(new JLabel(label), gbLabel);

		
		GridBagConstraints gbCampo = new GridBagConstraints();
		gbCampo.fill = GridBagConstraints.NONE;
		gbCampo.anchor = GridBagConstraints.NORTHWEST;
		gbCampo.insets = new Insets(2, 4, 2, 4);
		gbCampo.gridx = 0;
		gbCampo.gridy = ++row;
		gbCampo.weightx = 1.0;
		gbCampo.fill = GridBagConstraints.BOTH;
		gbCampo.gridwidth = GridBagConstraints.REMAINDER;
		this.getContentPane().add(txt, gbCampo);
		row++;
	}
	
	@SuppressWarnings("rawtypes")
	public void adicionaCombo(String label, JComboBox cb) {
		GridBagConstraints gbLabel = new GridBagConstraints();
		gbLabel.fill = GridBagConstraints.NONE;
		gbLabel.anchor = GridBagConstraints.NORTHWEST;
		gbLabel.insets = new Insets(2, 4, 2, 4);
		gbLabel.gridx = 2;
		gbLabel.gridy = row;
		gbLabel.weightx = 1.0;
		gbLabel.gridwidth = 1;
		this.getContentPane().add(new JLabel(label), gbLabel);
		
		GridBagConstraints gbCampo = new GridBagConstraints();
		gbCampo.fill = GridBagConstraints.NONE;
		gbCampo.anchor = GridBagConstraints.NORTHWEST;
		gbCampo.insets = new Insets(2, 4, 2, 4);
		gbCampo.gridx = 5;
		gbCampo.gridy = row;
		gbCampo.weightx = 1.0;
		gbCampo.fill = GridBagConstraints.BOTH;
		gbCampo.gridwidth = 1;
		this.getContentPane().add(cb, gbCampo);
		row++;
	}
	
	@SuppressWarnings("rawtypes")
	public void adicionaComboLinhaUnica(String label, JComboBox cb) {
		GridBagConstraints gbLabel = new GridBagConstraints();
		gbLabel.fill = GridBagConstraints.NONE;
		gbLabel.anchor = GridBagConstraints.NORTHWEST;
		gbLabel.insets = new Insets(2, 4, 2, 4);
		gbLabel.gridx = 1;
		gbLabel.gridy = row;
		gbLabel.weightx = 1.0;
		gbLabel.gridwidth = 1;
		this.getContentPane().add(new JLabel(label), gbLabel);
		
		GridBagConstraints gbCampo = new GridBagConstraints();
		gbCampo.fill = GridBagConstraints.NONE;
		gbCampo.anchor = GridBagConstraints.NORTHWEST;
		gbCampo.insets = new Insets(2, 4, 2, 4);
		gbCampo.gridx = 2;
		gbCampo.gridy = row;
		gbCampo.weightx = 1.0;
		gbCampo.fill = GridBagConstraints.BOTH;
		gbCampo.gridwidth = 3;
		this.getContentPane().add(cb, gbCampo);
		row++;
	}
	
	public void criaTelaInterna(String titulo, GridBagLayoutUtility comp) {
		comp.setTitle(titulo);
		int pointX = (getDesktopPane().getWidth() - comp.getWidth()) / 2;  
		int pointY = (getDesktopPane().getHeight() - comp.getHeight()) / 2;  
		comp.setLocation(pointX, pointY);
		comp.show();
		getDesktopPane().remove(comp);  
		getDesktopPane().add(comp);
		comp.toFront();
		if (!comp.isSelected()) {
			try {
				comp.setSelected(true);
			} catch (PropertyVetoException e) {
				e.printStackTrace();
			}
		}
	}
}



