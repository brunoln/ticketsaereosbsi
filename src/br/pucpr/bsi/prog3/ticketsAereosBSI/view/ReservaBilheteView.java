package br.pucpr.bsi.prog3.ticketsAereosBSI.view;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import br.pucpr.bsi.prog3.ticketsAereosBSI.bc.AeroportoBC;
import br.pucpr.bsi.prog3.ticketsAereosBSI.bc.PassageiroBC;
import br.pucpr.bsi.prog3.ticketsAereosBSI.bc.VooBC;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Aeroporto;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.CiaAerea;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Passageiro;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Voo;
import br.pucpr.bsi.prog3.ticketsAereosBSI.util.Funcoes;
import br.pucpr.bsi.prog3.ticketsAereosBSI.view.table.VooTable;

public class ReservaBilheteView extends GridBagLayoutUtility {

	private static final long serialVersionUID = 1L;
	
	private JComboBox<Passageiro> cbPassageiro = new JComboBox<Passageiro>();
	private JComboBox<Aeroporto> cbEmbarque = new JComboBox<Aeroporto>();
	private JComboBox<Aeroporto> cbDesembarque = new JComboBox<Aeroporto>();
	private JButton btCriarPassageiro = new JButton("Criar Passageiro");
	private JButton btConsulta = new JButton("Consulta");
	private PassageiroView passageiroView;	
	private JRadioButton ida = new JRadioButton("Ida", true);
	private JRadioButton idaVolta = new JRadioButton("ida_volta", false);
	private ButtonGroup radioGroup = new ButtonGroup();
	private JFormattedTextField txtDataPartida = new JFormattedTextField(Funcoes.setMascara("##/##/####"));
	private JFormattedTextField txtDataChegada = new JFormattedTextField(Funcoes.setMascara("##/##/####"));
	private VooTable tabela;
	private JScrollPane scrollPane;

	public ReservaBilheteView() {
		this("Reserva de Bilhetes", 500, 400);
	}
	
	public ReservaBilheteView(String title, int x, int y) {
		super(title, x, y);
		createScreen();
	}
	
	private void createScreen() {		
		montaComboBoxPassageiro();
		montaComboBoxEmbarque();
		montaComboBoxDesembarque();
		carregaTabelaTest();
		
		radioGroup.add(ida);
		radioGroup.add(idaVolta);
		tabela = new VooTable();
		scrollPane = new JScrollPane();
		scrollPane.setViewportView(tabela);
		scrollPane.setPreferredSize(new Dimension(200, 50));
	
		adicionaDoisCamposUltimoMenor("Passageiro:", cbPassageiro, "", btCriarPassageiro);
		adicionaDoisCampos("", ida, "", idaVolta);
		adicionaDoisCampos("Partida:", txtDataPartida, "Chegada:", txtDataChegada);
		adicionaDoisCampos("Embarque", cbEmbarque, "Desembarque", cbDesembarque);
		//add("", btConsulta);
		addTable(scrollPane);
		adicionaBotaoSalvarCancelarEndereco(btConsulta);
		btCriarPassageiro.addActionListener(new AbrePassageiroListener());
		ida.addActionListener(new idaListener());
		idaVolta.addActionListener(new IdaVoltaListener());
		txtDataChegada.setEnabled(false);
		
		btConsulta.addActionListener(new ConsultaListener());
	}

	private void montaComboBoxPassageiro() {
		cbPassageiro.removeAllItems();
		Passageiro passageiro3 = new Passageiro("huash", "YSAU", "ushaua", new Date(), null);
		Passageiro passageiro2 = new Passageiro("hua2222sh", "YSAU", "ushaua", new Date(), null);
		
		List<Passageiro> passageiros = PassageiroBC.getInstance().getList();
		
		passageiros.add(passageiro2);
		passageiros.add(passageiro3);
		for (Passageiro passageiro : passageiros) {
			cbPassageiro.addItem(passageiro);
		}
	}
	
	private void montaComboBoxEmbarque() {
		cbEmbarque.removeAllItems();
		List<Aeroporto> aeroportos = AeroportoBC.getInstance().getList();
		for (Aeroporto aeroporto : aeroportos) {
			cbEmbarque.addItem(aeroporto);
		}
	}
	
	private void montaComboBoxDesembarque() {
		cbDesembarque.removeAllItems();
		List<Aeroporto> aeroportos = AeroportoBC.getInstance().getList();
		for (Aeroporto aeroporto : aeroportos) {
			cbDesembarque.addItem(aeroporto);
		}
	}
	
	private class AbrePassageiroListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			passageiroView = new PassageiroView();
			criaTelaInterna("Passageiro", passageiroView);
		}
	}
	
	private class idaListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			txtDataChegada.setEnabled(false);
		}
	}
	
	private class IdaVoltaListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			txtDataChegada.setEnabled(true);
		}
	}
	
	private class ConsultaListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			SwingUtilities.invokeLater(newAtualizVooTable());
		}
	}

	public Runnable newAtualizVooTable() {
		return new Runnable() {
			public void run() {
					VooBC bc = VooBC.getInstance();
					tabela.reload(bc.getList());
			}
		};
	}
	
	public void carregaTabelaTest() {
		Endereco endereco = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");
		
		Aeroporto aeroportoCongonhas = new Aeroporto("Congonhas", "C1", endereco);
		Aeroporto aeroportoGuarulhos = new Aeroporto("Guarulhos", "C1", endereco);
		CiaAerea ciaAerea = new CiaAerea("TAM");
		Voo voo = new Voo("Curitiba", "Congonhas/Guarulhos", ciaAerea, aeroportoCongonhas, aeroportoGuarulhos);
		
		Voo voo2 = new Voo("São Paulo", "Guarulhos/Congonhas", ciaAerea, aeroportoGuarulhos, aeroportoCongonhas);
		
		VooBC voobc = VooBC.getInstance();
		voobc.create(voo);
		voobc.create(voo2);
	}

	public static void main(String[] args) {
		//Use an appropriate Look and Feel 
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (UnsupportedLookAndFeelException ex) {
			ex.printStackTrace();
		} catch (IllegalAccessException ex) {
			ex.printStackTrace();
		} catch (InstantiationException ex) {
			ex.printStackTrace();
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}
		//Schedule a job for the event dispatch thread:
		//creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				ReservaBilheteView pd = new ReservaBilheteView();
				pd.setVisible(true);
			}
		});
	}
	
}
