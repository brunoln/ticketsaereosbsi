package br.pucpr.bsi.prog3.ticketsAereosBSI.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import br.pucpr.bsi.prog3.ticketsAereosBSI.bc.EnderecoBC;
import br.pucpr.bsi.prog3.ticketsAereosBSI.inter.CriaEndereco;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Endereco;

public class EnderecoView extends GridBagLayoutUtility {
	private static final long serialVersionUID = 1L;
	private JTextField txtRua = new JTextField();
	private JTextField txtNumero = new JTextField();
	private JTextField txtComplemento = new JTextField();
	private JTextField txtBairro = new JTextField();
	private JTextField txtCidade = new JTextField();
	private JTextField txtEstado = new JTextField();
	private JTextField txtPais = new JTextField();

	private CriaEndereco comp;

	public EnderecoView(CriaEndereco comp) {
		this("Cadastro de Endereço", 400, 300);
		this.comp = comp;
	}

	public EnderecoView(String title, int x, int y) {
		super(title, x, y);
		createScreen();
	}

	private void createScreen() {
		adicionaDoisCamposUltimoMenor("Rua:", txtRua, "Número:", txtNumero);
		adicionaDoisCampos("Complemento:", txtComplemento, "Bairro:", txtBairro);
		adiciona("Cidade", txtCidade);
		adicionaDoisCampos("Estado:", txtEstado, "País:", txtPais);
		adicionaBotaoSalvarECancelar();
		okButton.addActionListener(new SalvarEnderecoListener());
	}

	private void resetForm() {
		txtRua.setText("");
		txtNumero.setText("");
		txtComplemento.setText("");
		txtBairro.setText("");
		txtCidade.setText("");
		txtEstado.setText("");
		txtPais.setText("");
	}

	private Endereco loadEnderecoFromPanel() {
		String rua = txtRua.getText().trim();
		String numero = txtNumero.getText().trim();
		String complemento = txtComplemento.getText().trim();
		String bairro = txtBairro.getText().trim();
		String cidade = txtCidade.getText().trim();
		String estado = txtEstado.getText().trim();
		String pais = txtPais.getText().trim();

		return new Endereco(rua, Integer.parseInt(numero), complemento, bairro,
				cidade, estado, pais);
	}

	private class SalvarEnderecoListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (comp != null) {
				try {
					Endereco m = loadEnderecoFromPanel();
					EnderecoBC bc = EnderecoBC.getInstance();
					long id = bc.create(m);
					m.setId(id);

					setVisible(false);
					resetForm();

					comp.setEndereco(m);
					comp.incluiRuaEndereco(m.getRua());
					comp.desabilitaBotaoEndereco();
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(EnderecoView.this,
							ex.getMessage(), "Erro ao salvar Aeroporto",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}
}
