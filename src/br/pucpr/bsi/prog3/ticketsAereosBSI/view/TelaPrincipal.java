package br.pucpr.bsi.prog3.ticketsAereosBSI.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GraphicsEnvironment;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.beans.PropertyVetoException;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JSplitPane;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import br.pucpr.bsi.prog3.ticketsAereosBSI.view.list.ListaDeAeroportos;

public class TelaPrincipal extends JFrame implements ActionListener, MouseListener, WindowListener {

	private PopupMenu popupMenu;

	private static final long serialVersionUID = 1L;

	public JMenuBar menuBar = new JMenuBar();
	
	public JMenu menuCadastroAereo = new javax.swing.JMenu("Cadastro Aéreo");
	
	public JMenu menuCadastroPessoa = new javax.swing.JMenu("Cadastro Pessoa");
	
	public JMenu menuBilhetes = new javax.swing.JMenu("Bilhetes");
	
	public JMenu menuAjuda = new javax.swing.JMenu("Ajuda");
	
	public JMenuItem menuSobre = new javax.swing.JMenuItem("Sobre");
	
	public JMenuItem menuAeroporto = new javax.swing.JMenuItem("Aeroporto");
	
	public JMenuItem menuAviao = new javax.swing.JMenuItem("Avião");
	
	public JMenuItem menuPassageiro = new javax.swing.JMenuItem("Passageiro");
	
	public JMenuItem menuListaAeroporto = new javax.swing.JMenuItem("Aeroporto");
	
	public JMenuItem menuCiaAerea = new javax.swing.JMenuItem("Cia Aérea");
	
	public JMenuItem menuPapel = new javax.swing.JMenuItem("Papel");
	
	public JMenuItem menuFuncionario = new javax.swing.JMenuItem("Funcionário");
	
	public JMenuItem menuVoo = new javax.swing.JMenuItem("Vôo");
	
	public JMenuItem menuHorario = new javax.swing.JMenuItem("Horário de Vôo");
	
	public JMenuItem menuReservaBilhete = new javax.swing.JMenuItem("Reserva");
	
	public JMenuItem menuCompraBilhete = new javax.swing.JMenuItem("Compra");

	private Rectangle posicao_tela = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices()[0].getDefaultConfiguration().getBounds();

	public Container c = getContentPane();

	public JDesktopPane dpArea = new JDesktopPane();

	private ImageIcon icFundo = null;

	private JLabel lbFundo = null;

	private int iWidthImgFundo = 0;

	private int iHeightImgFundo = 0;

	public Color padrao = new Color(145, 167, 208);

	public String sImgFundo = null;

	private JSplitPane splitPane = null;
	
	public TelaPrincipal(String title) {
		super(title);
		
		c.setLayout(new BorderLayout());
		
		criaMenuBar();
		criaSplitPane();
		
		c.add(splitPane, BorderLayout.CENTER);
		setExtendedState(MAXIMIZED_BOTH);
		inicializaTela();

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				fecharJanela();
			}
		});
		// Adicona o Listener para tratar eventos da janela
		addWindowListener(this);
	}
	
	private void criaMenuBar() {
		
		//item de menu Aeroporto
		menuListaAeroporto.setText("Aeroporto");
		menuCadastroAereo.add(menuListaAeroporto);
		menuListaAeroporto.setAccelerator(KeyStroke.getKeyStroke(
		        KeyEvent.VK_A, ActionEvent.ALT_MASK));
		
		//item de menu Vôo
		menuCadastroAereo.add(menuVoo);
		menuVoo.setAccelerator(KeyStroke.getKeyStroke(
		        KeyEvent.VK_V, ActionEvent.ALT_MASK));
		
		//item de menu Avião
		menuCadastroAereo.add(menuAviao);
		menuAviao.setAccelerator(KeyStroke.getKeyStroke(
		        KeyEvent.VK_I, ActionEvent.ALT_MASK));
		
		//item de menu Horário de Vôo
		menuCadastroAereo.add(menuHorario);
		menuHorario.setAccelerator(KeyStroke.getKeyStroke(
		        KeyEvent.VK_H, ActionEvent.ALT_MASK));
		
		//item de menu Cia Aérea
        menuCadastroAereo.add(menuCiaAerea);
        menuCiaAerea.setAccelerator(KeyStroke.getKeyStroke(
		        KeyEvent.VK_O, ActionEvent.ALT_MASK));
        
        //item de menu Passageiro
        menuCadastroPessoa.add(menuPassageiro);
        menuPassageiro.setAccelerator(KeyStroke.getKeyStroke(
		        KeyEvent.VK_P, ActionEvent.ALT_MASK));
        
        //item de menu Funcionário
        menuCadastroPessoa.add(menuFuncionario);
        menuFuncionario.setAccelerator(KeyStroke.getKeyStroke(
		        KeyEvent.VK_F, ActionEvent.ALT_MASK));
        
        //item de menu Papel
        menuCadastroPessoa.add(menuPapel);
        menuPapel.setAccelerator(KeyStroke.getKeyStroke(
		        KeyEvent.VK_L, ActionEvent.ALT_MASK));
        
        //item de menu Reserva
        menuBilhetes.add(menuReservaBilhete);
        menuReservaBilhete.setAccelerator(KeyStroke.getKeyStroke(
		        KeyEvent.VK_R, ActionEvent.ALT_MASK));
        
        //item de menu Compra
        menuBilhetes.add(menuCompraBilhete);
        menuCompraBilhete.setAccelerator(KeyStroke.getKeyStroke(
		        KeyEvent.VK_C, ActionEvent.ALT_MASK));
        
        //Menu Sobre
        menuAjuda.add(menuSobre);
        menuSobre.setAccelerator(KeyStroke.getKeyStroke(
		        KeyEvent.VK_S, ActionEvent.ALT_MASK));
        
        menuBar.add(menuCadastroAereo);
        menuBar.add(menuCadastroPessoa);
        menuBar.add(menuBilhetes);
        menuBar.add(menuAjuda);
        
		setJMenuBar(menuBar);
		
		menuAviao.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				criatela("Cadastro de Avião", new AviaoView());
			}
		});
		
		final TelaPrincipal tela = this;
		menuListaAeroporto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				criatela("Lista de Aeroportos", new ListaDeAeroportos(tela), "");
			}
		});
		
		menuCiaAerea.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				criatela("Cadastro de Cia Aérea", new CiaAereaView());
			}
		});
		
		menuPapel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				criatela("Cadastro de papel", new PapelView());
			}
		});
		
		menuFuncionario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				criatela("Cadastro de funcionário", new FuncionarioView());
			}
		});
		
		menuPassageiro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				criatela("Cadastro de Passageiro", new PassageiroView());
			}
		});
		
		menuVoo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				criatela("Cadastro de Voo", new VooView());
			}
		});
		
		menuHorario.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				criatela("Cadastro de Horário", new HorarioView());
			}
		});
		
		menuReservaBilhete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				criatela("Reserva de Bilhetes ", new ReservaBilheteView());
			}
		});
		
		menuCompraBilhete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				criatela("Compra de Bilhetes ", new CompraBilheteView());
			}
		});
		
		menuSobre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				criatela("Sobre a aplicação", new Sobre(), "");
			}
		});
	}
	
	private void criaSplitPane() {
		splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		splitPane.setContinuousLayout(true);
		splitPane.setOneTouchExpandable(true);
		splitPane.setTopComponent(dpArea);
		splitPane.setDividerSize(1);
	}

	protected void adicionarItemMenu(MenuItem mi) {
		this.popupMenu.add(mi);
	}

	public void mouseClicked(MouseEvent arg0) {
	}

	public void mouseEntered(MouseEvent arg0) {
	}

	public void mouseExited(MouseEvent arg0) {
	}

	public void mousePressed(MouseEvent arg0) {
	}

	public void mouseReleased(MouseEvent arg0) {
	}

	public void addKeyListerExterno(KeyListener arg0) {
		this.addKeyListener(arg0);
	}

	public void adicFilha(Container filha) {
		dpArea.add(filha);
		
	}

	public void criatela(String titulo, GridBagLayoutUtility comp) {
		comp.setTitle(titulo);
		int pointX = (dpArea.getWidth() - comp.getWidth()) / 2;  
		int pointY = (dpArea.getHeight() - comp.getHeight()) / 2;  
		comp.setLocation(pointX, pointY);
		comp.show();
		adicFilha(comp);
		comp.toFront();
		if (!comp.isSelected()) {
			try {
				comp.setSelected(true);
			} catch (PropertyVetoException e) {
				e.printStackTrace();
			}
		}

	}
	
	public void criatela(String titulo, JInternalFrame comp, String diff) {
		comp.setTitle(titulo);
		int pointX = (dpArea.getWidth() - comp.getWidth()) / 2;  
		int pointY = (dpArea.getHeight() - comp.getHeight()) / 2;  
		comp.setLocation(pointX, pointY);
		comp.show();
		
		adicFilha(comp);
		comp.toFront();
		if (!comp.isSelected()) {
			try {
				comp.setSelected(true);
			} catch (PropertyVetoException e) {
				e.printStackTrace();
			}
		}

	}
		
	
	/*public void criatela(String titulo, JFrame comp) {
		comp.setTitle(titulo);
		comp.setLocationRelativeTo(c);
		comp.setResizable(false);
		
		comp.setVisible(true);
	}*/
	
	public void setBgColor(Color cor) {
		if (cor == null)
			cor = padrao;
		dpArea.setBackground(cor);
	}

	public void reposicionaImagens() {
		try {
			posicao_tela = this.getBounds();
			final int iWidthArea = ( int ) posicao_tela.getWidth();
			final int iHeightArea = ( int ) posicao_tela.getHeight();

			lbFundo.setBounds(( iWidthArea / 2 ) - ( lbFundo.getWidth() / 2 ), 
							(( iHeightArea - 200 ) / 2 ) - ( lbFundo.getHeight() / 2 ), 
							lbFundo.getWidth(), lbFundo.getHeight());
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void addFundo() {
		addFundo(null);
	}

	public void addFundo(JComponent comp) {
		final int iWidthArea = ( int ) posicao_tela.getWidth();
		final int iHeightArea = ( int ) posicao_tela.getHeight();
		setSize(iWidthArea, iHeightArea - 50);

		if (comp == null) {
			lbFundo = new JLabel(icFundo);
			lbFundo.setSize(iWidthImgFundo, iHeightImgFundo);
			comp = lbFundo;
		}

		comp.setBounds(( iWidthArea / 2 ) - ( comp.getWidth() / 2 ), ( ( iHeightArea - 200 ) / 2 )
				- ( comp.getHeight() / 2 ), comp.getWidth(), comp.getHeight());

		dpArea.add(comp);
	}

	
	public void inicializaTela() {
		addFundo();
		setBgColor(padrao);
	}

	public void fecharJanela() {
		System.exit(0);
	}
	
	public void windowOpen() {
	   System.out.println("Janela aberta");	
	}
	
	@Override
	public void windowActivated(WindowEvent arg0) {
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		windowOpen();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
	}
	

	public static void main(String[] args) {
		//Use an appropriate Look and Feel 
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (UnsupportedLookAndFeelException ex) {
			ex.printStackTrace();
		} catch (IllegalAccessException ex) {
			ex.printStackTrace();
		} catch (InstantiationException ex) {
			ex.printStackTrace();
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}
		//Schedule a job for the event dispatch thread:
		//creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				TelaPrincipal pd = new TelaPrincipal("Ticket Aereos BSI");
				pd.setVisible(true);
				//new PopulaTabelas();
			}
		});
	}
}
