package br.pucpr.bsi.prog3.ticketsAereosBSI.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import br.pucpr.bsi.prog3.ticketsAereosBSI.bc.CiaAereaBC;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.CiaAerea;

public class CiaAereaView extends GridBagLayoutUtility{
	
	private static final long serialVersionUID = 1L;
	private JTextField txtNome = new JTextField();
	
	public CiaAereaView() {
		this("Cadastro de Cia Aérea", 400, 130);
	}

	public CiaAereaView(String title, int x, int y) {
		super(title, x, y);
		createScreen();
		
		okButton.addActionListener(new SalvarCiaAereaListener());
	}
	
	private void createScreen() {
		adiciona("Nome", txtNome);

		addButtonOKandClose(this);
	}
	
	private void resetForm() {	
		txtNome.setText("");
	}
	
	private class SalvarCiaAereaListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				CiaAerea m = new CiaAerea(txtNome.getText());
				CiaAereaBC bc = CiaAereaBC.getInstance();
				bc.create(m);
				setVisible(false);
				resetForm();
			} catch(Exception ex) {
				JOptionPane.showMessageDialog(CiaAereaView.this, 
						ex.getMessage(), "Erro ao salvar Avião", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

}
