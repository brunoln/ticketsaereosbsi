package br.pucpr.bsi.prog3.ticketsAereosBSI.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.pucpr.bsi.prog3.ticketsAereosBSI.conn.Conexao;
import br.pucpr.bsi.prog3.ticketsAereosBSI.exception.PersistenceException;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Aviao;

public class AviaoDAO {
	public final static String SELECT_AVIAO_BY_ID = "SELECT * FROM AVIAO WHERE ID=?";
	public final static String INSERT_AVIAO = "INSERT INTO AVIAO (CODIGO, CARGA) VALUES (?,?)";
	public final static String UPDATE_AVIAO = "UPDATE AVIAO SET CODIGO=?, CARGA=? WHERE ID=?";
	public final static String LISTA_AVIOES = "SELECT * FROM AVIAO";
	public final static String DELETE_AVIAO = "DELETE FROM AVIAO WHERE ID=?";

	public AviaoDAO() {
	}
	
	public Aviao getAviaoByID(long id) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Aviao aviao = null;
		try {
			conn = Conexao.getInstance().obterConexao();
			stmt = conn.prepareStatement(SELECT_AVIAO_BY_ID);
			rs = stmt.executeQuery();
			
			if (rs.next()) {
				String codigo = rs.getString("codigo");
				double carga = rs.getInt("carga");
				aviao = new Aviao(codigo, carga, null);
				aviao.setId(id);
			}

		} catch (SQLException e) {
			String errorMsg = "Erro ao consultar todas os aviões!";
			throw new PersistenceException(errorMsg, e);
		} finally {
			Conexao.closeAll(conn, stmt, rs);
		}

		return aviao;
		
	}

	public long salve(Aviao aviao) {
		Connection conn = Conexao.getInstance().obterConexao();
		PreparedStatement stmt = null;
		Integer id = null;
		String sql = INSERT_AVIAO;

		try  {
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			int i = 1;
			stmt.setString(i++, aviao.getCodigo());
			stmt.setDouble(i++, aviao.getCarga());
			stmt.executeUpdate();

			try (ResultSet rs = stmt.getGeneratedKeys()) {
				if (rs.next()) {
					id = rs.getInt("id");
				}
				rs.close();
			}
			conn.commit();
		} catch (SQLException e) {
			String errorMsg = "Erro ao inserir avião!";
			try {
				conn.rollback();
			} catch (SQLException e1) {
				System.err.println("Erro no rollback - AviaoDao");
			}
			throw new PersistenceException(errorMsg, e);
		} finally {
			Conexao.closeAll(conn, stmt);
		}
		return id;
	}	
	
	public boolean editar(Aviao aviao) {
		Connection conn = Conexao.getInstance().obterConexao();
		PreparedStatement stmt = null;
		String sql = UPDATE_AVIAO;

		try  {
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);

			int i = 1;
			stmt.setString(i++, aviao.getCodigo());
			stmt.setDouble(i++, aviao.getCarga());
			stmt.setLong(i++, aviao.getId());
			stmt.executeUpdate();
			
			conn.commit();
		} catch (SQLException e) {
			String errorMsg = "Erro ao editar avião!";
			try {
				conn.rollback();
			} catch (SQLException e1) {
				System.err.println("Erro no rollback - AviaoDao");
			}
			throw new PersistenceException(errorMsg, e);
		} finally {
			Conexao.closeAll(conn, stmt);
		}
		return true;
	}	
	
	public boolean excluir(long id) {
		Connection conn = Conexao.getInstance().obterConexao();
		PreparedStatement stmt = null;
		String sql = DELETE_AVIAO;

		try  {
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);

			int i = 1;
			stmt.setLong(i++, id);
			stmt.executeUpdate();
			
			conn.commit();
		} catch (SQLException e) {
			String errorMsg = "Erro ao excluir avião!";
			try {
				conn.rollback();
			} catch (SQLException e1) {
				System.err.println("Erro no rollback - AviaoDao");
			}
			throw new PersistenceException(errorMsg, e);
		} finally {
			Conexao.closeAll(conn, stmt);
		}
		return true;
	}	

	public List<Aviao> getList() {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<Aviao> lista = null;

		try {
			conn = Conexao.getInstance().obterConexao();
			stmt = conn.prepareStatement(LISTA_AVIOES);
			rs = stmt.executeQuery();
			lista = new ArrayList<Aviao>();

			while (rs.next()) {
				long id = rs.getLong("id");
				String codigo = rs.getString("codigo");
				double carga = rs.getInt("carga");
				Aviao aviao = new Aviao(codigo, carga, null);
				aviao.setId(id);
				lista.add(aviao);
			}

		} catch (SQLException e) {
			String errorMsg = "Erro ao consultar todas os aviões!";
			throw new PersistenceException(errorMsg, e);
		} finally {
			Conexao.closeAll(conn, stmt, rs);
		}

		return lista;
	}
}
