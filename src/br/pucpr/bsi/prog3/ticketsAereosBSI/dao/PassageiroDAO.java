package br.pucpr.bsi.prog3.ticketsAereosBSI.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.pucpr.bsi.prog3.ticketsAereosBSI.conn.Conexao;
import br.pucpr.bsi.prog3.ticketsAereosBSI.exception.PersistenceException;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Passageiro;
import br.pucpr.bsi.prog3.ticketsAereosBSI.util.Funcoes;

public class PassageiroDAO {
	public final static String SELECT_PASSAGEIRO_BY_ID = "select p.*, e.* from passageiro p, endereco e where id=? and e.id = p.id_endereco";
	public final static String INSERT_PASSAGEIRO = "insert into passageiro (id_endereco, nome, email, telefone, data_nascimento, numero_cartao, documento) values (?, ?, ?, ?, ?, ?, ?)";
	public final static String UPDATE_PASSAGEIRO = "update passageiro set id_endereco=?, nome=?, email=?, telefone=?, data_nascimento=?, numero_cartao=?, documento=? where id=?";
	public final static String LISTA_PASSAGEIRO = "select p.*, e.* from passageiro p, endereco e where e.id = p.id_endereco";
	public final static String DELETE_PASSAGEIRO = "delete from passageiro where id=?";

	public PassageiroDAO() {
	}

	public Passageiro getPassageiroByID(long id) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Passageiro passageiro = null;
		try {
			conn = Conexao.getInstance().obterConexao();
			stmt = conn.prepareStatement(SELECT_PASSAGEIRO_BY_ID);
			rs = stmt.executeQuery();

			if (rs.next()) {

				String nome = rs.getString("nome");
				String email = rs.getString("email");
				String telefone = rs.getString("telefone");
				Date dataNascimento = Funcoes.sqlDateToDate(rs
						.getDate("data_nascimento"));
				String numeroCartao = rs.getString("numero_cartao");
				String documento = rs.getString("documento");

				long idEndereco = rs.getLong("id_endereco");
				String rua = rs.getString("rua");
				Integer numero = rs.getInt("numero");
				String complemento = rs.getString("complemento");
				String bairro = rs.getString("bairro");
				String estado = rs.getString("estado");
				String pais = rs.getString("pais");
				String cidade = rs.getString("cidade");
				Endereco endereco = new Endereco(rua, numero, complemento,
						bairro, cidade, estado, pais);
				endereco.setId(idEndereco);

				passageiro = new Passageiro(nome, email, telefone,
						dataNascimento, endereco);
				passageiro.setNumeroCartao(numeroCartao);
				passageiro.setDocumento(documento);
				passageiro.setId(id);
			}

		} catch (SQLException e) {
			String errorMsg = "Erro ao consultar passageiro!";
			throw new PersistenceException(errorMsg, e);
		} finally {
			Conexao.closeAll(conn, stmt, rs);
		}

		return passageiro;

	}

	public long salve(Passageiro passageiro) {
		Connection conn = Conexao.getInstance().obterConexao();
		PreparedStatement stmt = null;
		Integer id = null;
		String sql = INSERT_PASSAGEIRO;

		try {
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			int param = 1;
			stmt.setLong(param++, passageiro.getEndereco().getId());
			stmt.setString(param++, passageiro.getNome());
			stmt.setString(param++, passageiro.getEmail());
			stmt.setString(param++, passageiro.getTelefone());
			stmt.setDate(param++,
					Funcoes.dateToSQLDate(passageiro.getDataNascimento()));
			stmt.setString(param++, passageiro.getNumeroCartao());
			stmt.setString(param++, passageiro.getDocumento());

			stmt.executeUpdate();

			try (ResultSet rs = stmt.getGeneratedKeys()) {
				if (rs.next()) {
					id = rs.getInt("id");
				}
				rs.close();
			}
			conn.commit();
		} catch (SQLException e) {
			String errorMsg = "Erro ao inserir passageiro!";
			try {
				conn.rollback();
			} catch (SQLException e1) {
				System.err.println("Erro no rollback - PassageiroDAO");
			}
			throw new PersistenceException(errorMsg, e);
		} finally {
			Conexao.closeAll(conn, stmt);
		}
		return id;
	}

	public boolean editar(Passageiro passageiro) {
		Connection conn = Conexao.getInstance().obterConexao();
		PreparedStatement stmt = null;
		String sql = UPDATE_PASSAGEIRO;

		try {
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);

			int param = 1;
			stmt.setLong(param++, passageiro.getEndereco().getId());
			stmt.setString(param++, passageiro.getNome());
			stmt.setString(param++, passageiro.getEmail());
			stmt.setString(param++, passageiro.getTelefone());
			stmt.setDate(param++, Funcoes.dateToSQLDate(passageiro.getDataNascimento()));
			stmt.setString(param++, passageiro.getNumeroCartao());
			stmt.setString(param++, passageiro.getDocumento());

			stmt.setLong(param++, passageiro.getId());

			stmt.executeUpdate();

			conn.commit();
		} catch (SQLException e) {
			String errorMsg = "Erro ao editar passageiro!";
			try {
				conn.rollback();
			} catch (SQLException e1) {
				System.err.println("Erro no rollback - PassageiroDao");
			}
			throw new PersistenceException(errorMsg, e);
		} finally {
			Conexao.closeAll(conn, stmt);
		}
		return true;
	}

	public boolean excluir(long id) {
		Connection conn = Conexao.getInstance().obterConexao();
		PreparedStatement stmt = null;
		String sql = DELETE_PASSAGEIRO;

		try {
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);

			int i = 1;
			stmt.setLong(i++, id);
			stmt.executeUpdate();

			conn.commit();
		} catch (SQLException e) {
			String errorMsg = "Erro ao excluir Passageiro!";
			try {
				conn.rollback();
			} catch (SQLException e1) {
				System.err.println("Erro no rollback - PassageiroDAO");
			}
			throw new PersistenceException(errorMsg, e);
		} finally {
			Conexao.closeAll(conn, stmt);
		}
		return true;
	}

	public List<Passageiro> getList() {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<Passageiro> lista = null;

		try {
			conn = Conexao.getInstance().obterConexao();
			stmt = conn.prepareStatement(LISTA_PASSAGEIRO);
			rs = stmt.executeQuery();
			lista = new ArrayList<Passageiro>();

			while (rs.next()) {
				long id = rs.getLong("id");
				String nome = rs.getString("nome");
				String email = rs.getString("email");
				String telefone = rs.getString("telefone");
				Date dataNascimento = Funcoes.sqlDateToDate(rs
						.getDate("data_nascimento"));
				String numeroCartao = rs.getString("numero_cartao");
				String documento = rs.getString("documento");

				long idEndereco = rs.getLong("id_endereco");
				String rua = rs.getString("rua");
				Integer numero = rs.getInt("numero");
				String complemento = rs.getString("complemento");
				String bairro = rs.getString("bairro");
				String estado = rs.getString("estado");
				String pais = rs.getString("pais");
				String cidade = rs.getString("cidade");
				Endereco endereco = new Endereco(rua, numero, complemento,
						bairro, cidade, estado, pais);
				endereco.setId(idEndereco);

				Passageiro passageiro = new Passageiro(nome, email, telefone,
						dataNascimento, endereco);
				passageiro.setNumeroCartao(numeroCartao);
				passageiro.setDocumento(documento);
				passageiro.setId(id);
				lista.add(passageiro);
			}

		} catch (SQLException e) {
			String errorMsg = "Erro ao consultar todos os Aeroportos!";
			throw new PersistenceException(errorMsg, e);
		} finally {
			Conexao.closeAll(conn, stmt, rs);
		}
		return lista;
	}
}
