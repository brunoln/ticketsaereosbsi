package br.pucpr.bsi.prog3.ticketsAereosBSI.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.pucpr.bsi.prog3.ticketsAereosBSI.conn.Conexao;
import br.pucpr.bsi.prog3.ticketsAereosBSI.exception.PersistenceException;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.CiaAerea;

public class CiaAereaDAO {
	public final static String SELECT_CIAAEREA_BY_NOME = "SELECT * FROM CIA_AEREA WHERE NOME=?";
	public final static String INSERT_CIAAEREA = "INSERT INTO CIA_AEREA (NOME) VALUES (?)";
	public final static String UPDATE_CIAAEREA = "UPDATE CIA_AEREA SET NOME=? WHERE ID=?";
	public final static String LISTA_CIAAEREAS = "SELECT * FROM CIA_AEREA";
	public final static String DELETE_CIAAEREA = "DELETE FROM CIA_AEREA WHERE ID=?";

	public CiaAereaDAO() {
	}

	public long salve(CiaAerea cia) {
		Connection conn = Conexao.getInstance().obterConexao();
		PreparedStatement stmt = null;
		Integer id = null;
		String sql = INSERT_CIAAEREA;

		try  {
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			int i = 1;
			stmt.setString(i++, cia.getNome());
			stmt.executeUpdate();

			try (ResultSet rs = stmt.getGeneratedKeys()) {
				if (rs.next()) {
					id = rs.getInt("id");
				}
				rs.close();
			}
			conn.commit();
		} catch (SQLException e) {
			String errorMsg = "Erro ao inserir cia aerea!";
			try {
				conn.rollback();
			} catch (SQLException e1) {
				System.err.println("Erro no rollback - CiaAereaDAO");
			}
			throw new PersistenceException(errorMsg, e);
		} finally {
			Conexao.closeAll(conn, stmt);
		}
		return id;
	}	
	
	public boolean editar(CiaAerea cia) {
		Connection conn = Conexao.getInstance().obterConexao();
		PreparedStatement stmt = null;
		String sql = UPDATE_CIAAEREA;

		try  {
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);

			int i = 1;
			stmt.setString(i++, cia.getNome());
			stmt.setLong(i++, cia.getId());
			stmt.executeUpdate();
			
			conn.commit();
		} catch (SQLException e) {
			String errorMsg = "Erro ao editar CiaAerea!";
			try {
				conn.rollback();
			} catch (SQLException e1) {
				System.err.println("Erro no rollback - CiaAereaDAO");
			}
			throw new PersistenceException(errorMsg, e);
		} finally {
			Conexao.closeAll(conn, stmt);
		}
		return true;
	}	
	
	public boolean excluir(CiaAerea cia) {
		Connection conn = Conexao.getInstance().obterConexao();
		PreparedStatement stmt = null;
		String sql = DELETE_CIAAEREA;

		try  {
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);

			int i = 1;
			stmt.setLong(i++, cia.getId());
			stmt.executeUpdate();
			
			conn.commit();
		} catch (SQLException e) {
			String errorMsg = "Erro ao excluir CiaAerea!";
			try {
				conn.rollback();
			} catch (SQLException e1) {
				System.err.println("Erro no rollback - CiaAereaDAO");
			}
			throw new PersistenceException(errorMsg, e);
		} finally {
			Conexao.closeAll(conn, stmt);
		}
		return true;
	}	

	public List<CiaAerea> getList() {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<CiaAerea> lista = null;

		try {
			conn = Conexao.getInstance().obterConexao();
			stmt = conn.prepareStatement(LISTA_CIAAEREAS);
			rs = stmt.executeQuery();
			lista = new ArrayList<CiaAerea>();

			while (rs.next()) {
				long id = rs.getInt("id");
				String nome = rs.getString("nome");
				CiaAerea cia = new CiaAerea(nome);
				cia.setId(id);
				lista.add(cia);
			}

		} catch (SQLException e) {
			String errorMsg = "Erro ao consultar todas os cia aereas!";
			throw new PersistenceException(errorMsg, e);
		} finally {
			Conexao.closeAll(conn, stmt, rs);
		}
		return lista;
	}

}
