package br.pucpr.bsi.prog3.ticketsAereosBSI.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.pucpr.bsi.prog3.ticketsAereosBSI.conn.Conexao;
import br.pucpr.bsi.prog3.ticketsAereosBSI.exception.PersistenceException;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Aeroporto;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Endereco;

public class AeroportoDAO {
	public final static String SELECT_AEROPORTO_BY_ID = "select a.*, e.* from aeroporto a, endereco e where id=? and e.id = a.id_endereco";
	public final static String INSERT_AEROPORTO = "insert into aeroporto (id_endereco, nome, codigo) values (?, ?, ?)";
	public final static String UPDATE_AEROPORTO = "update aeroporto set id_endereco=?, nome=?, codigo=? where id=?";
	public final static String LISTA_AEROPORTOS = "select * from aeroporto a, endereco e where e.id = a.id_endereco";
	public final static String DELETE_AEROPORTO = "delete from aeroporto where id=?";

	public AeroportoDAO() {
	}

	public Aeroporto getAeroportoByID(long id) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Aeroporto aeroporto = null;
		try {
			conn = Conexao.getInstance().obterConexao();
			stmt = conn.prepareStatement(SELECT_AEROPORTO_BY_ID);
			rs = stmt.executeQuery();

			if (rs.next()) {

				String nome = rs.getString("nome");
				String codigo = rs.getString("codigo");

				long idEndereco = rs.getLong("id_endereco");
				String rua = rs.getString("rua");
				Integer numero = rs.getInt("numero");
				String complemento = rs.getString("complemento");
				String bairro = rs.getString("bairro");
				String estado = rs.getString("estado");
				String pais = rs.getString("pais");
				String cidade = rs.getString("cidade");
				Endereco endereco = new Endereco(rua, numero, complemento,
						bairro, cidade, estado, pais);
				endereco.setId(idEndereco);

				aeroporto = new Aeroporto(nome, codigo, endereco);
				aeroporto.setId(id);
			}

		} catch (SQLException e) {
			String errorMsg = "Erro ao consultar aeroporto!";
			throw new PersistenceException(errorMsg, e);
		} finally {
			Conexao.closeAll(conn, stmt, rs);
		}

		return aeroporto;

	}

	public long salve(Aeroporto aeroporto) {
		Connection conn = Conexao.getInstance().obterConexao();
		PreparedStatement stmt = null;
		Integer id = null;
		String sql = INSERT_AEROPORTO;

		try {
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			int param = 1;
			stmt.setLong(param++, aeroporto.getEndereco().getId());
			stmt.setString(param++, aeroporto.getNome());
			stmt.setString(param++, aeroporto.getCodigo());
			stmt.executeUpdate();

			try (ResultSet rs = stmt.getGeneratedKeys()) {
				if (rs.next()) {
					id = rs.getInt("id");
				}
				rs.close();
			}
			conn.commit();
		} catch (SQLException e) {
			String errorMsg = "Erro ao inserir aeroporto!";
			try {
				conn.rollback();
			} catch (SQLException e1) {
				System.err.println("Erro no rollback - AeroportoDao");
			}
			throw new PersistenceException(errorMsg, e);
		} finally {
			Conexao.closeAll(conn, stmt);
		}
		return id;
	}

	public boolean editar(Aeroporto aeroporto) {
		Connection conn = Conexao.getInstance().obterConexao();
		PreparedStatement stmt = null;
		String sql = UPDATE_AEROPORTO;

		try {
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);

			int param = 1;
			stmt.setLong(param++, aeroporto.getEndereco().getId());
			stmt.setString(param++, aeroporto.getNome());
			stmt.setString(param++, aeroporto.getCodigo());

			stmt.executeUpdate();

			conn.commit();
		} catch (SQLException e) {
			String errorMsg = "Erro ao editar aeroporto!";
			try {
				conn.rollback();
			} catch (SQLException e1) {
				System.err.println("Erro no rollback - AeroportoDao");
			}
			throw new PersistenceException(errorMsg, e);
		} finally {
			Conexao.closeAll(conn, stmt);
		}
		return true;
	}

	public boolean excluir(long id) {
		Connection conn = Conexao.getInstance().obterConexao();
		PreparedStatement stmt = null;
		String sql = DELETE_AEROPORTO;

		try {
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);

			int i = 1;
			stmt.setLong(i++, id);
			stmt.executeUpdate();

			conn.commit();
		} catch (SQLException e) {
			String errorMsg = "Erro ao excluir Aeroporto!";
			try {
				conn.rollback();
			} catch (SQLException e1) {
				System.err.println("Erro no rollback - AeroportoDao");
			}
			throw new PersistenceException(errorMsg, e);
		} finally {
			Conexao.closeAll(conn, stmt);
		}
		return true;
	}

	public List<Aeroporto> getList() {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<Aeroporto> lista = null;

		try {
			conn = Conexao.getInstance().obterConexao();
			stmt = conn.prepareStatement(LISTA_AEROPORTOS);
			rs = stmt.executeQuery();
			lista = new ArrayList<Aeroporto>();

			while (rs.next()) {
				long id = rs.getLong("id");
				String nome = rs.getString("nome");
				String codigo = rs.getString("codigo");

				long idEndereco = rs.getLong("id_endereco");
				String rua = rs.getString("rua");
				Integer numero = rs.getInt("numero");
				String complemento = rs.getString("complemento");
				String bairro = rs.getString("bairro");
				String estado = rs.getString("estado");
				String pais = rs.getString("pais");
				String cidade = rs.getString("cidade");
				Endereco endereco = new Endereco(rua, numero, complemento,
						bairro, cidade, estado, pais);
				endereco.setId(idEndereco);

				Aeroporto aerporto = new Aeroporto(nome, codigo, endereco);
				aerporto.setId(id);
				lista.add(aerporto);
			}

		} catch (SQLException e) {
			String errorMsg = "Erro ao consultar todos os Aeroportos!";
			throw new PersistenceException(errorMsg, e);
		} finally {
			Conexao.closeAll(conn, stmt, rs);
		}
		return lista;
	}
}
