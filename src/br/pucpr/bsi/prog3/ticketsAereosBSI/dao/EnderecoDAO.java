package br.pucpr.bsi.prog3.ticketsAereosBSI.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.pucpr.bsi.prog3.ticketsAereosBSI.conn.Conexao;
import br.pucpr.bsi.prog3.ticketsAereosBSI.exception.PersistenceException;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Endereco;

public class EnderecoDAO {
	public final static String SELECT_ENDERECO_BY_ID = "select * from endereco where id=?";
	public final static String INSERT_ENDERECO = "insert into endereco(rua, numero, complemento, bairro, cidade, estado, pais) values (?, ?, ?, ?, ?, ?, ?)";
	public final static String UPDATE_ENDERECO = "update endereco set rua=?, complemento=?, bairro=?, cidade=?, estado=?, pais=? where id=?";
	public final static String LISTA_ENDERECOS = "select * from endereco";
	public final static String DELETE_ENDERECO = "delete from endereco where id=?";

	public EnderecoDAO() {
	}
	
	public Endereco getEnderecoByID(long id) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Endereco endereco = null;
		try {
			conn = Conexao.getInstance().obterConexao();
			stmt = conn.prepareStatement(SELECT_ENDERECO_BY_ID);
			rs = stmt.executeQuery();
			
			if (rs.next()) {
				String rua = rs.getString("rua");
				Integer numero = rs.getInt("numero");
				String complemento = rs.getString("complemento");
				String bairro = rs.getString("bairro");
				String estado = rs.getString("estado");
				String pais = rs.getString("pais");
				String cidade = rs.getString("cidade");
				endereco = new Endereco(rua, numero, complemento, bairro, cidade, estado, pais);
				endereco.setId(id);
			}

		} catch (SQLException e) {
			String errorMsg = "Erro ao consultar endereço!";
			throw new PersistenceException(errorMsg, e);
		} finally {
			Conexao.closeAll(conn, stmt, rs);
		}

		return endereco;
		
	}

	public long salve(Endereco endereco) {
		Connection conn = Conexao.getInstance().obterConexao();
		PreparedStatement stmt = null;
		Integer id = null;
		String sql = INSERT_ENDERECO;

		try  {
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			int param = 1;
			stmt.setString(param++, endereco.getRua());
			stmt.setInt(param++, endereco.getNumero());
			stmt.setString(param++, endereco.getComplemento());
			stmt.setString(param++, endereco.getBairro());
			stmt.setString(param++, endereco.getCidade());
			stmt.setString(param++, endereco.getEstado());
			stmt.setString(param++, endereco.getPais());
			stmt.executeUpdate();

			try (ResultSet rs = stmt.getGeneratedKeys()) {
				if (rs.next()) {
					id = rs.getInt("id");
				}
				rs.close();
			}
			conn.commit();
		} catch (SQLException e) {
			String errorMsg = "Erro ao inserir endereço!";
			try {
				conn.rollback();
			} catch (SQLException e1) {
				System.err.println("Erro no rollback - EnderecoDao");
			}
			throw new PersistenceException(errorMsg, e);
		} finally {
			Conexao.closeAll(conn, stmt);
		}
		return id;
	}	
	
	public boolean editar(Endereco endereco) {
		Connection conn = Conexao.getInstance().obterConexao();
		PreparedStatement stmt = null;
		String sql = UPDATE_ENDERECO;

		try  {
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);

			int param = 1;
			stmt.setString(param++, endereco.getRua());
			stmt.setInt(param++, endereco.getNumero());
			stmt.setString(param++, endereco.getComplemento());
			stmt.setString(param++, endereco.getCidade());
			stmt.setString(param++, endereco.getEstado());
			stmt.setString(param++, endereco.getPais());
			
			stmt.setLong(param++, endereco.getId());
			
			stmt.executeUpdate();
			
			conn.commit();
		} catch (SQLException e) {
			String errorMsg = "Erro ao editar endereco!";
			try {
				conn.rollback();
			} catch (SQLException e1) {
				System.err.println("Erro no rollback - EnderecoDao");
			}
			throw new PersistenceException(errorMsg, e);
		} finally {
			Conexao.closeAll(conn, stmt);
		}
		return true;
	}	
	
	public boolean excluir(long id) {
		Connection conn = Conexao.getInstance().obterConexao();
		PreparedStatement stmt = null;
		String sql = DELETE_ENDERECO;

		try  {
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);

			int i = 1;
			stmt.setLong(i++, id);
			stmt.executeUpdate();
			
			conn.commit();
		} catch (SQLException e) {
			String errorMsg = "Erro ao excluir Endereço!";
			try {
				conn.rollback();
			} catch (SQLException e1) {
				System.err.println("Erro no rollback - EnderecoDao");
			}
			throw new PersistenceException(errorMsg, e);
		} finally {
			Conexao.closeAll(conn, stmt);
		}
		return true;
	}	

	public List<Endereco> getList() {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<Endereco> lista = null;

		try {
			conn = Conexao.getInstance().obterConexao();
			stmt = conn.prepareStatement(LISTA_ENDERECOS);
			rs = stmt.executeQuery();
			lista = new ArrayList<Endereco>();

			while (rs.next()) {
				long id = rs.getLong("id");
				String rua = rs.getString("rua");
				Integer numero = rs.getInt("numero");
				String complemento = rs.getString("complemento");
				String bairro = rs.getString("bairro");
				String estado = rs.getString("estado");
				String pais = rs.getString("pais");
				String cidade = rs.getString("cidade");
				Endereco endereco = new Endereco(rua, numero, complemento, bairro, cidade, estado, pais);
				endereco.setId(id);
				lista.add(endereco);
			}

		} catch (SQLException e) {
			String errorMsg = "Erro ao consultar todas os Endereços!";
			throw new PersistenceException(errorMsg, e);
		} finally {
			Conexao.closeAll(conn, stmt, rs);
		}
		return lista;
	}
}
