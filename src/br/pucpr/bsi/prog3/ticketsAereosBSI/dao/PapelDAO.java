package br.pucpr.bsi.prog3.ticketsAereosBSI.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.pucpr.bsi.prog3.ticketsAereosBSI.conn.Conexao;
import br.pucpr.bsi.prog3.ticketsAereosBSI.exception.PersistenceException;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.CiaAerea;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Papel;

public class PapelDAO {
	public final static String SELECT_PAPEL_BY_ID = "select p.*, c.nome nome_cia from papel p, cia_aerea c where  c.id = p.id_cia_aerea";
	public final static String INSERT_PAPEL = "insert into papel(id_cia_aerea, nome, descricao) values (?,?,?)";
	public final static String UPDATE_PAPEL = "update aviao set id_cia_aerea=?, nome=?, descricao=? where id=?";
	public final static String LISTA_PAPEIS = "select p.*, c.nome nome_cia from papel p, cia_aerea c where  c.id = p.id_cia_aerea";
	public final static String DELETE_PAPEL = "delete from papel where id=?";

	public PapelDAO() {
	}
	
	public Papel getPapelByID(long id) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Papel papel = null;
		try {
			conn = Conexao.getInstance().obterConexao();
			stmt = conn.prepareStatement(SELECT_PAPEL_BY_ID);
			rs = stmt.executeQuery();
			
			if (rs.next()) {
				String nome = rs.getString("nome");
				String descricao = rs.getString("descricao");
				
				//Dados CiaAerea
				long idCiaAerea = rs.getLong("id_cia_aerea");
				String nomeCia = rs.getString("nome_cia");
				CiaAerea ciaAerea = new CiaAerea(nomeCia);
				ciaAerea.setId(idCiaAerea);
				
				papel = new Papel(nome, descricao, ciaAerea);
				papel.setId(id);
			}

		} catch (SQLException e) {
			String errorMsg = "Erro ao consultar todas os papeis!";
			throw new PersistenceException(errorMsg, e);
		} finally {
			Conexao.closeAll(conn, stmt, rs);
		}

		return papel;
		
	}

	public long salve(Papel papel) {
		Connection conn = Conexao.getInstance().obterConexao();
		PreparedStatement stmt = null;
		Integer id = null;
		String sql = INSERT_PAPEL;

		try  {
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			int param = 1;
			stmt.setLong(param++, papel.getCiaAerea().getId());
			stmt.setString(param++, papel.getNome());
			stmt.setString(param++, papel.getDescricao());
			stmt.executeUpdate();

			try (ResultSet rs = stmt.getGeneratedKeys()) {
				if (rs.next()) {
					id = rs.getInt("id");
				}
				rs.close();
			}
			conn.commit();
		} catch (SQLException e) {
			String errorMsg = "Erro ao inserir Papel!";
			try {
				conn.rollback();
			} catch (SQLException e1) {
				System.err.println("Erro no rollback - PapelDao");
			}
			throw new PersistenceException(errorMsg, e);
		} finally {
			Conexao.closeAll(conn, stmt);
		}
		return id;
	}	
	
	public boolean editar(Papel papel) {
		Connection conn = Conexao.getInstance().obterConexao();
		PreparedStatement stmt = null;
		String sql = UPDATE_PAPEL;

		try  {
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);

			int param = 1;
			stmt.setString(param++, papel.getNome());
			stmt.setString(param++, papel.getDescricao());
			stmt.setLong(param++, papel.getCiaAerea().getId());
			stmt.setLong(param++, papel.getId());
			stmt.executeUpdate();
			
			conn.commit();
		} catch (SQLException e) {
			String errorMsg = "Erro ao editar papel!";
			try {
				conn.rollback();
			} catch (SQLException e1) {
				System.err.println("Erro no rollback - PapelDao");
			}
			throw new PersistenceException(errorMsg, e);
		} finally {
			Conexao.closeAll(conn, stmt);
		}
		return true;
	}	
	
	public boolean excluir(long id) {
		Connection conn = Conexao.getInstance().obterConexao();
		PreparedStatement stmt = null;
		String sql = DELETE_PAPEL;

		try  {
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);

			int param = 1;
			stmt.setLong(param++, id);
			stmt.executeUpdate();
			
			conn.commit();
		} catch (SQLException e) {
			String errorMsg = "Erro ao excluir papel!";
			try {
				conn.rollback();
			} catch (SQLException e1) {
				System.err.println("Erro no rollback - PapelDao");
			}
			throw new PersistenceException(errorMsg, e);
		} finally {
			Conexao.closeAll(conn, stmt);
		}
		return true;
	}	

	public List<Papel> getList() {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<Papel> lista = null;

		try {
			conn = Conexao.getInstance().obterConexao();
			stmt = conn.prepareStatement(LISTA_PAPEIS);
			rs = stmt.executeQuery();
			lista = new ArrayList<Papel>();

			while (rs.next()) {
				long id = rs.getLong("id");
				String nome = rs.getString("nome");
				String descricao = rs.getString("descricao");
				
				//Dados CiaAerea
				long idCiaAerea = rs.getLong("id_cia_aerea");
				String nomeCia = rs.getString("nome_cia");
				CiaAerea ciaAerea = new CiaAerea(nomeCia);
				ciaAerea.setId(idCiaAerea);
				
				Papel papel = new Papel(nome, descricao, ciaAerea);
				papel.setId(id);
				lista.add(papel);
			}

		} catch (SQLException e) {
			String errorMsg = "Erro ao consultar todas os aviões!";
			throw new PersistenceException(errorMsg, e);
		} finally {
			Conexao.closeAll(conn, stmt, rs);
		}

		return lista;
	}
}
