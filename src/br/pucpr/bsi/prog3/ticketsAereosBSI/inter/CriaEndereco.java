package br.pucpr.bsi.prog3.ticketsAereosBSI.inter;

import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Endereco;

public interface CriaEndereco {
	public void setEndereco(Endereco endereco);
	public void incluiRuaEndereco(String text);
	public void desabilitaBotaoEndereco();
}
