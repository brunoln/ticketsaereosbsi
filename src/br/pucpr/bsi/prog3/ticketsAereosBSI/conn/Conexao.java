package br.pucpr.bsi.prog3.ticketsAereosBSI.conn;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.sql.DataSource;

import org.hsqldb.jdbc.JDBCPool;

public class Conexao {

	private static final String ARQ_CONF = "resources/conexao.conf";
	private static Conexao instancia = new Conexao();
	private Properties configuracoes;
	private final DataSource dataSource;

	private Conexao() {
		System.out.print("iniciando connection pool...");
		lerConfiguracoes();
		JDBCPool pool = new JDBCPool();
		pool.setUrl(configuracoes.getProperty("url"));
		pool.setUser(configuracoes.getProperty("usuario"));
		pool.setPassword(configuracoes.getProperty("senha"));
		dataSource = pool;
		System.out.println(" ok");
	}

	private void lerConfiguracoes() {
		try {
			File arqConf = new File(ARQ_CONF);
			if (!arqConf.exists())
				throw new IllegalArgumentException(
						"Arquivo de configuração não existe " + ARQ_CONF);
			FileInputStream entrada = new FileInputStream(arqConf);
			configuracoes = new Properties();
			configuracoes.load(entrada);
			entrada.close();
		} catch (IOException e) {
			throw new RuntimeException("Erro de E/S",e);
		}
	}

	public Connection obterConexao() {
		try {
			Connection connection = dataSource.getConnection();
			return connection;
		} catch (SQLException e) {
			throw new RuntimeException("Erro ao criar conexão", e);
		}
	}

	public static Conexao getInstance() {
		return instancia;
	}

	public static void closeAll(Connection conn) {
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (Exception e) {
			System.err.println("Não foi possivel fechar a conexão!");
		}
	}

	public static void closeAll(Connection conn, Statement stmt) {
		try {
			if (conn != null) {
				closeAll(conn);
			}
			if (stmt != null) {
				stmt.close();
			}
		} catch (Exception e) {
			System.err.println("Não foi possivel fechar o Statement!");
		}
	}

	public static void closeAll(Connection conn, Statement stmt, ResultSet rs) {
		try {
			if (conn != null || stmt != null) {
				closeAll(conn, stmt);
			}
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			System.err.println("Não foi possivel fechar o resultSet!");
		}
	}

}