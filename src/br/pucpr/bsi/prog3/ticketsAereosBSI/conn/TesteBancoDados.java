package br.pucpr.bsi.prog3.ticketsAereosBSI.conn;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class TesteBancoDados {
	private Connection connection = null;
	private Statement statement = null;
	private ResultSet resultSet = null;
	public TesteBancoDados(){
		try {
			connection = Conexao.getInstance().obterConexao();
			JOptionPane.showMessageDialog(null,"Conectou");
			//Permite realizar Roolbacks caso ocorra erro durante o execute
			connection.setAutoCommit(false);
			statement = connection.createStatement();
			statement.executeUpdate(
					"CREATE TABLE TB_PESSOA(ID INTEGER, NOME VARCHAR(30))");
			statement.executeUpdate(
					"INSERT INTO TB_PESSOA(ID, NOME) VALUES (1,'ação teste')");
			statement.executeUpdate(
					"INSERT INTO TB_PESSOA(ID, NOME) VALUES (2,'cleudiclei farias')");
			statement.executeUpdate(
					"INSERT INTO TB_PESSOA(ID, NOME) VALUES (3,'mercelinda ferreira')");
			statement.executeUpdate(
					"INSERT INTO TB_PESSOA(ID, NOME) VALUES (4,'alfred sorttemberg')");
			//Comita as informacoes
			connection.commit();
			statement = connection.createStatement();
			resultSet = statement.executeQuery("select * from TB_PESSOA");
			while(resultSet.next()){
				System.out.println(resultSet.getString("nome"));
			}
			statement.executeUpdate("DROP TABLE TB_PESSOA");
		} catch (Exception e) {
			try {
				if(connection != null && !connection.isClosed()) {
					connection.rollback();
				}
			} catch (SQLException e2) {
				e2.printStackTrace();
			}

			return;
		} finally {
			try {
				if(resultSet != null){
					resultSet.close();
				}
				if(statement != null){
					statement.close();
				}
				if(connection != null){
					connection.close();
				}
			} catch (SQLException e) {
				//Nada a fazer
			}
		}
	}
	public static void main(String args[]){
		new TesteBancoDados();
	}
}