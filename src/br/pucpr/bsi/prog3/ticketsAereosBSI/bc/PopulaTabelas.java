package br.pucpr.bsi.prog3.ticketsAereosBSI.bc;
import java.util.Date;

import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Aeroporto;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Aviao;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Bilhete;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.CiaAerea;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Economica;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Funcionario;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Horario;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Papel;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Passageiro;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.SituacaoBilheteEnum;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Voo;


public class PopulaTabelas {
	public PopulaTabelas() {
		
		//cria endereços
		Endereco endereco = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");
		

		Endereco endereco2 = new Endereco("Rua alferes Poli 2", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");

		EnderecoBC enderecoBC = EnderecoBC.getInstance();
		enderecoBC.create(endereco);
		enderecoBC.create(endereco2);
		
		
		//cria passageiros
		Passageiro passageiro = new Passageiro("Bruno Nascimento", "b@email.com",
				"5555-5555", new Date(), endereco);
		
		Passageiro passageiro2 = new Passageiro("Caroline Nardino", "c@email.com",
				"5555-5555", new Date(), endereco2);
		
		PassageiroBC passageiroBC = PassageiroBC.getInstance();
		passageiroBC.create(passageiro);
		passageiroBC.create(passageiro2);
		
		
		//Cria funcionarios
		Funcionario funcionario = new Funcionario( "Bruno Nascimento", "b@email.com",
				"5555-5555", new Date(), endereco);
		
		Funcionario funcionario2 = new Funcionario( "Caroline Nardino", "c@email.com",
				"5555-5555", new Date(), endereco);
		FuncionarioBC funcionarioBC = FuncionarioBC.getInstance();
		funcionarioBC.create(funcionario);
		funcionarioBC.create(funcionario2);
		
		
		//Cria Aeroporto
		Aeroporto aeroporto = new Aeroporto("Congonhas", "C1", endereco);
		Aeroporto aeroporto2 = new Aeroporto("Curitiba", "C2", endereco2);
		
		AeroportoBC aeroportoBC = AeroportoBC.getInstance();
		aeroportoBC.create(aeroporto);
		aeroportoBC.create(aeroporto2);
		
		
		//Cria ciaAerea
		CiaAerea ciaAerea = new CiaAerea("TAM");
		CiaAerea ciaAerea2 = new CiaAerea("GOL");
		
		CiaAereaBC ciaAereaBC = CiaAereaBC.getInstance();
		ciaAereaBC.create(ciaAerea);
		ciaAereaBC.create(ciaAerea2);
		
		//Cria Papel
		Papel papel = new Papel("Piloto1", "Pilota avioes",ciaAerea);
		Papel papel2 = new Papel("Piloto2", "Pilota avioes",ciaAerea);
		
		PapelBC papelBC = PapelBC.getInstance();
		papelBC.create(papel);
		papelBC.create(papel2);
		
		//cria Bilhete
		Bilhete bilhete = new Economica(passageiro, SituacaoBilheteEnum.DISPONIVEL);
		Bilhete bilhete2 = new Economica(passageiro2, SituacaoBilheteEnum.RESERVADO);
		
		BilheteBC bilheteBC = BilheteBC.getInstance();
		bilheteBC.create(bilhete);
		bilheteBC.create(bilhete2);
		
		
		//Cria Avião		
		Aviao aviao = new Aviao("Boing TAM", 2, null);
		Aviao aviao2 = new Aviao("Boing GOL", 1, null);
		AviaoBC aviaoBC = AviaoBC.getInstance();
		aviaoBC.create(aviao);
		aviaoBC.create(aviao2);
		
		
		//cria Voo
		Voo voo = new Voo("Curitiba", "CWB-SP", ciaAerea, aeroporto, aeroporto2);
		Voo voo2 = new Voo("Curitiba", "SP-CWB", ciaAerea, aeroporto2, aeroporto);
		
		VooBC vooBC = VooBC.getInstance();
		vooBC.create(voo);
		vooBC.create(voo2);
		
		
		//cria horário
		Horario horario = new Horario(aviao, voo,"codigo", new Date(), new Date(), 10, 10, 10);
		Horario horario2 = new Horario(aviao2, voo2,"codigo2", new Date(), new Date(), 10, 10, 10);
		
		HorarioBC horarioBC = HorarioBC.getInstance();
		horarioBC.create(horario);
		horarioBC.create(horario2);
		
	}
}
