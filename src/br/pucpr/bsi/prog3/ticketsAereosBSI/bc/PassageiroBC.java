package br.pucpr.bsi.prog3.ticketsAereosBSI.bc;

import java.util.List;

import br.pucpr.bsi.prog3.ticketsAereosBSI.dao.PassageiroDAO;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Passageiro;

public class PassageiroBC {
	private static PassageiroBC instance;
	private PassageiroDAO passageiroDAO;

	private PassageiroBC() {
		passageiroDAO = new PassageiroDAO();
	}

	public synchronized long create(Passageiro passageiro) {

		return passageiroDAO.salve(passageiro);
	}

	public synchronized Passageiro retrieve(long key) {
		return passageiroDAO.getPassageiroByID(key);
	}

	public synchronized boolean update(Passageiro passageiro) {
		if (passageiro != null && passageiro.getId() != 0)
			return false;

		return passageiroDAO.editar(passageiro);
	}

	public synchronized boolean delete(Passageiro passageiro) {
		if (passageiro != null && passageiro.getId() != 0)
			return false;

		return passageiroDAO.excluir(passageiro.getId());
	}

	public List<Passageiro> getList() {
		return passageiroDAO.getList();
	}

	public static PassageiroBC getInstance() {
		if (instance == null) {
			instance = new PassageiroBC();
		}
		return instance;
	}
}
