package br.pucpr.bsi.prog3.ticketsAereosBSI.bc;

import java.util.List;

import br.pucpr.bsi.prog3.ticketsAereosBSI.dao.EnderecoDAO;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Endereco;

public class EnderecoBC {
	private static EnderecoBC instance;
	private EnderecoDAO enderecoDao;

	private EnderecoBC() {
		enderecoDao = new EnderecoDAO();
	}

	public synchronized long create(Endereco endereco) {
		return enderecoDao.salve(endereco);
	}

	public synchronized Endereco retrieve(long key) {
		return enderecoDao.getEnderecoByID(key);
	}

	public synchronized boolean update(Endereco endereco) {
		if (endereco != null && endereco.getId() != 0)
			return false;

		return enderecoDao.editar(endereco);
	}

	public synchronized boolean delete(Endereco endereco) {
		if (endereco != null && endereco.getId() != 0)
			return false;

		return enderecoDao.excluir(endereco.getId());
	}

	public List<Endereco> getList() {
		return enderecoDao.getList();
	}

	public static EnderecoBC getInstance() {
		if (instance == null) {
			instance = new EnderecoBC();
		}
		return instance;
	}
}
