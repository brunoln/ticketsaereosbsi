package br.pucpr.bsi.prog3.ticketsAereosBSI.bc;

import java.util.List;

import br.pucpr.bsi.prog3.ticketsAereosBSI.dao.CiaAereaDAO;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.CiaAerea;

public class CiaAereaBC {
	private static CiaAereaBC instance;
	CiaAereaDAO dao = null;
	
	//private long contadorId = 0;
	
	//private Map<Long, CiaAerea> tabela = new HashMap<Long, CiaAerea>();
	
	private CiaAereaBC() {
		dao = new CiaAereaDAO();
	}
	
	public synchronized long create(CiaAerea ciaAerea) {
		return dao.salve(ciaAerea);
	}
	
	/*public synchronized CiaAerea retrieve(long key) {
		return tabela.get(key);
	}
	
	public synchronized boolean update(CiaAerea ciaAerea ) {
		if (ciaAerea != null && !tabela.containsKey(ciaAerea.getId())) 
			return false;

		return tabela.put(ciaAerea.getId(), ciaAerea) != null;
	}
	
	public synchronized boolean delete(CiaAerea ciaAerea) {
		if (ciaAerea != null && !tabela.containsKey(ciaAerea.getId())) 
			return false;

		return tabela.remove(ciaAerea.getId()) != null;
	}*/
	
	public static CiaAereaBC getInstance(){
		if(instance == null){
			instance = new CiaAereaBC();
		}
		return instance;
	}
	
	public List<CiaAerea> getList() {
		return dao.getList();
	}
}
