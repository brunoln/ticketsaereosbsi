
package br.pucpr.bsi.prog3.ticketsAereosBSI.bc;

import java.util.HashMap;
import java.util.Map;

import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Horario;

public class HorarioBC {
	private static HorarioBC instance;
	
	private long contadorId = 0;
	
	private Map<Long, Horario> tabela = new HashMap<Long, Horario>();
	
	private HorarioBC() {}
	
	public synchronized long create(Horario horario) {
		horario.setId(contadorId);
		tabela.put(horario.getId(), horario);
		return contadorId++;
	}
	
	public synchronized Horario retrieve(long key) {
		return tabela.get(key);
	}
	
	public synchronized boolean update(Horario horario) {
		if (horario != null && !tabela.containsKey(horario.getId())) 
			return false;

		return tabela.put(horario.getId(), horario) != null;
	}
	
	public synchronized boolean delete(Horario horario) {
		if (horario != null && !tabela.containsKey(horario.getId())) 
			return false;

		return tabela.remove(horario.getId()) != null;
	}
	
	public static HorarioBC getInstance(){
		if(instance == null){
			instance = new HorarioBC();
		}
		return instance;
	}
}
