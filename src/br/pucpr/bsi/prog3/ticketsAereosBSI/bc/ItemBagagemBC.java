package br.pucpr.bsi.prog3.ticketsAereosBSI.bc;

import java.util.HashMap;
import java.util.Map;

import br.pucpr.bsi.prog3.ticketsAereosBSI.model.ItemBagagem;

public class ItemBagagemBC {
	private static ItemBagagemBC instance;
	
	private long contadorId = 0;
	
	private Map<Long, ItemBagagem> tabela = new HashMap<Long, ItemBagagem>();
	
	private ItemBagagemBC() {}
	
	public synchronized long create(ItemBagagem itemBagagem) {
		itemBagagem.setId(contadorId);
		tabela.put(itemBagagem.getId(), itemBagagem);
		return contadorId++;
	}
	
	public synchronized ItemBagagem retrieve(long key) {
		return tabela.get(key);
	}
	
	public synchronized boolean update(long key, ItemBagagem itemBagagem ) {
		if (itemBagagem != null && !tabela.containsKey(itemBagagem.getId())) 
			return false;

		return tabela.put(itemBagagem.getId(), itemBagagem) != null;
	}
	
	public synchronized boolean delete(ItemBagagem itemBagagem) {
		if (itemBagagem != null && !tabela.containsKey(itemBagagem.getId())) 
			return false;
		
		return tabela.remove(itemBagagem.getId()) != null;
	}
	
	public static ItemBagagemBC getInstance(){
		if(instance == null){
			instance = new ItemBagagemBC();
		}
		return instance;
	}
}
