package br.pucpr.bsi.prog3.ticketsAereosBSI.bc;

import java.util.List;

import br.pucpr.bsi.prog3.ticketsAereosBSI.dao.PapelDAO;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.CiaAerea;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Papel;

public class PapelBC {
	private static PapelBC instance;
	private PapelDAO papelDao;

	private PapelBC() {
		papelDao = new PapelDAO();
	}

	public synchronized long create(Papel papel) {
		return papelDao.salve(papel);
	}

	public synchronized Papel retrieve(long key) {
		return papelDao.getPapelByID(key);
	}

	public synchronized boolean update(Papel papel) {
		if (papel != null && papel.getId() != 0)
			return false;

		return papelDao.editar(papel);
	}

	public synchronized boolean delete(Papel papel) {
		if (papel != null && papel.getId() != 0)
			return false;

		return papelDao.excluir(papel.getId());
	}

	public static PapelBC getInstance() {
		if (instance == null) {
			instance = new PapelBC();
		}
		return instance;
	}

	public List<Papel> getList(CiaAerea ciaAerea) {
		return papelDao.getList();
	}

}
