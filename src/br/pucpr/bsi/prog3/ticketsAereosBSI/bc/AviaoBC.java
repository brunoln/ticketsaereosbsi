package br.pucpr.bsi.prog3.ticketsAereosBSI.bc;

import java.util.List;

import br.pucpr.bsi.prog3.ticketsAereosBSI.dao.AviaoDAO;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Aviao;

public class AviaoBC {
	private static AviaoBC instance;
	AviaoDAO dao = null;
	
	private AviaoBC() {
		dao = new AviaoDAO();
	}
	
	public synchronized long create(Aviao aviao) {
		return dao.salve(aviao);
	}
	
	public synchronized Aviao retrieve(long key) {
		return dao.getAviaoByID(key);
	}
	
	public synchronized boolean update(Aviao aviao) {
		if (aviao != null && aviao.getId() != 0) {
			return false;
		}
		return dao.editar(aviao);
	}
	
	public synchronized boolean delete(Aviao aviao) {
		if (aviao != null && aviao.getId() != 0) {
			return false;
		}
		return dao.excluir(aviao.getId());
	}
	
	public static AviaoBC getInstance(){
		if(instance == null){
			instance = new AviaoBC();
		}
		return instance;
	}
	
	public List<Aviao> getList() {
		return dao.getList();
	}
}
