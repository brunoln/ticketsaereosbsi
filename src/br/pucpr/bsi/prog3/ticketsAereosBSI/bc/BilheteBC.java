package br.pucpr.bsi.prog3.ticketsAereosBSI.bc;

import java.util.HashMap;
import java.util.Map;

import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Bilhete;

public class BilheteBC {
	private static BilheteBC instance;

	private long contadorId = 0;

	private Map<Long, Bilhete> tabela = new HashMap<Long, Bilhete>();

	private BilheteBC() {}

	public synchronized long create(Bilhete bilhete) {
		bilhete.setId(contadorId);
		tabela.put(bilhete.getId(), bilhete);
		return contadorId++;
	}

	public synchronized Bilhete retrieve(long key) {
		return tabela.get(key);
	}

	public synchronized boolean update(Bilhete bilhete ) {
		if (bilhete != null && !tabela.containsKey(bilhete.getId())) 
			return false;

		return tabela.put(bilhete.getId(), bilhete ) != null;
	}

	public synchronized boolean delete(Bilhete bilhete) {
		if (bilhete != null && !tabela.containsKey(bilhete.getId())) 
			return false;
		
		return tabela.remove(bilhete.getId()) != null;
	}

	public static BilheteBC getInstance(){
		if(instance == null){
			instance = new BilheteBC();
		}
		return instance;
	}
}
