package br.pucpr.bsi.prog3.ticketsAereosBSI.bc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Voo;

public class VooBC {
	private static VooBC instance;
	
	private long contadorId = 0;
	
	private Map<Long, Voo> tabela = new HashMap<Long, Voo>();
	
	private VooBC() {}
	
	public synchronized long create(Voo voo) {
		voo.setId(contadorId);
		tabela.put(voo.getId(), voo);
		return contadorId++;
	}
	
	public synchronized Voo retrieve(long key) {
		return tabela.get(key);
	}
	
	public synchronized boolean update(Voo voo) {
		if (voo != null && !tabela.containsKey(voo.getId())) 
			return false;

		return tabela.put(voo.getId(), voo) != null;
	}
	
	public synchronized boolean delete(Voo voo) {
		if (voo != null && !tabela.containsKey(voo.getId())) 
			return false;

		return tabela.remove(voo.getId()) != null;
	}
	
	public static VooBC getInstance(){
		if(instance == null){
			instance = new VooBC();
		}
		return instance;
	}
	
	public ArrayList<Voo> getList() {
		ArrayList<Voo> voos = new ArrayList<Voo>();
		Set<Entry<Long, Voo>> VooSet = tabela.entrySet();
		Iterator<Map.Entry<Long, Voo>> iter = VooSet.iterator();  
		while(iter.hasNext())  
		{  
			Voo voo = iter.next().getValue();  
			voos.add(voo); 
		}
		return voos;
	}
}