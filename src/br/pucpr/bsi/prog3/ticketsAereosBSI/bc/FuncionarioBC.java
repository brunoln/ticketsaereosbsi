package br.pucpr.bsi.prog3.ticketsAereosBSI.bc;

import java.util.HashMap;
import java.util.Map;

import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Funcionario;

public class FuncionarioBC {
	private static FuncionarioBC instance;
	
	private long contadorId = 0;
	
	private Map<Long, Funcionario> tabela = new HashMap<Long, Funcionario>();
	
	private FuncionarioBC() {}
	
	public synchronized long create(Funcionario funcionario) {
		funcionario.setId(contadorId);
		tabela.put(funcionario.getId(), funcionario);
		return contadorId++;
	}
	
	public synchronized Funcionario retrieve(long key) {
		return tabela.get(key);
	}
	
	public synchronized boolean update(Funcionario funcionario ) {
		if (funcionario != null && !tabela.containsKey(funcionario.getId())) 
			return false;
		
		return tabela.put(funcionario.getId(), funcionario) != null;
	}
	
	public synchronized boolean delete(Funcionario funcionario) {
		if (funcionario != null && !tabela.containsKey(funcionario.getId())) 
			return false;

		return tabela.remove(funcionario.getId()) != null;
	}
	
	public static FuncionarioBC getInstance(){
		if(instance == null){
			instance = new FuncionarioBC();
		}
		return instance;
	}
}