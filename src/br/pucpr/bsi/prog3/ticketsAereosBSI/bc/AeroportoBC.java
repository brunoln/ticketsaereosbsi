package br.pucpr.bsi.prog3.ticketsAereosBSI.bc;

import java.util.List;

import br.pucpr.bsi.prog3.ticketsAereosBSI.dao.AeroportoDAO;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Aeroporto;

public class AeroportoBC {
	private static AeroportoBC instance;
	private AeroportoDAO aeroportoDAO;

	private AeroportoBC() {
		aeroportoDAO = new AeroportoDAO();
	}

	public synchronized long create(Aeroporto aeroporto) {
		return aeroportoDAO.salve(aeroporto);
	}

	public synchronized Aeroporto retrieve(long key) {
		return aeroportoDAO.getAeroportoByID(key);
	}

	public synchronized boolean update(Aeroporto aeroporto) {
		if (aeroporto != null && aeroporto.getId() != 0)
			return false;

		return aeroportoDAO.editar(aeroporto);
	}

	public synchronized boolean delete(Aeroporto aeroporto) {
		if (aeroporto != null && aeroporto.getId() != 0)
			return false;

		return aeroportoDAO.excluir(aeroporto.getId());
	}

	public static AeroportoBC getInstance() {
		if (instance == null) {
			instance = new AeroportoBC();
		}
		return instance;
	}

	public List<Aeroporto> getList() {

		return aeroportoDAO.getList();
	}
}