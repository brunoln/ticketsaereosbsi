package br.pucpr.bsi.prog3.ticketsAereosBSI.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.text.MaskFormatter;

public class Funcoes {

	public static Date strDateToDate(String dataTexto) {
		Date data = null;
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			format.setLenient(false);
			data = format.parse(dataTexto);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return data;
	}

	public static MaskFormatter setMascara(String mascara) {
		MaskFormatter mask = null;
		try {
			mask = new MaskFormatter(mascara);
		} catch (java.text.ParseException ex) {
		}
		return mask;
	}

	public static Date sqlDateToDate(java.sql.Date dVal) {
		Date dRetorno = null;
		if (dVal != null) {
			dRetorno = new Date(dVal.getTime());
		}
		return dRetorno;
	}

	public static java.sql.Date dateToSQLDate(Date d) {
		return new java.sql.Date(d.getTime());
	}

}
