package br.pucpr.bsi.prog3.ticketsAereosBSI.model;

import java.util.ArrayList;

public class Executiva extends Bilhete {

	public Executiva(Passageiro passageiro, SituacaoBilheteEnum situacaoBilheteEnum) {
		super(passageiro, situacaoBilheteEnum);
		bagagens = new ArrayList<ItemBagagem>(2);
	}	
}
