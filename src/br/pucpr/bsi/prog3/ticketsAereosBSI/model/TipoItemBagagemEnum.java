package br.pucpr.bsi.prog3.ticketsAereosBSI.model;

public enum TipoItemBagagemEnum {
	MAO(1, "mão", 5.0), MALA(2,"mala", 22.00);
	
	private final int id;
	private final String nome;
	private final double pesoMax;
	
	TipoItemBagagemEnum(int id, String nome, double pesoMax) {
		this.id = id;
		this.nome = nome;
		this.pesoMax = pesoMax;
	}

	public int getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}
	
	public double getPesoMax() {
		return pesoMax;
	}
}