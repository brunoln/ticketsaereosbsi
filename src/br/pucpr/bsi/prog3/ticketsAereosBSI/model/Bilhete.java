
package br.pucpr.bsi.prog3.ticketsAereosBSI.model;

import java.util.Date;
import java.util.List;

public abstract class Bilhete {
	
	private long id;
	private int numero;
	private Date horario;
	private String assento;
	private Passageiro passageiro;
	private SituacaoBilheteEnum situacaoBilheteEnum;
	protected List<ItemBagagem> bagagens;
	
	public Bilhete(Passageiro passageiro, SituacaoBilheteEnum situacaoBilheteEnum) {
		this.passageiro = passageiro;
		this.situacaoBilheteEnum = situacaoBilheteEnum;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public Date getHorario() {
		return horario;
	}
	public void setHorario(Date horario) {
		this.horario = horario;
	}
	public String getAssento() {
		return assento;
	}
	public void setAssento(String assento) {
		this.assento = assento;
	}
	public List<ItemBagagem> getBagagens() {
		return bagagens;
	}
	public void setBagagens(List<ItemBagagem> bagagens) {
		this.bagagens = bagagens;
	}
	public SituacaoBilheteEnum getSituacaoBilheteEnum() {
		return situacaoBilheteEnum;
	}

	public Passageiro getPassageiro() {
		return passageiro;
	}

	public void setPassageiro(Passageiro passageiro) {
		this.passageiro = passageiro;
	}
}
