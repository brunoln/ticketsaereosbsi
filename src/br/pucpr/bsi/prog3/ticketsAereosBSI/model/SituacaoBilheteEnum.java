package br.pucpr.bsi.prog3.ticketsAereosBSI.model;

public enum SituacaoBilheteEnum {
	DISPONIVEL(1, "disponivel"), VENDIDO(2,"vendido"), RESERVADO(3, "reservado");
	
	private final int id;
	private final String nome;
	
	SituacaoBilheteEnum(int id, String nome) {
		this.id = id;
		this.nome = nome;
	}

	public int getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}
}
