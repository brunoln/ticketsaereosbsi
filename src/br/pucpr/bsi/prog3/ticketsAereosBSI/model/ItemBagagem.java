package br.pucpr.bsi.prog3.ticketsAereosBSI.model;

public class ItemBagagem {
	
	private long id;
	private Bilhete bilhete;
	private TipoItemBagagemEnum tipoItemBagagemEnum;
	
	public ItemBagagem(Bilhete bilhete, TipoItemBagagemEnum tipoItemBagagemEnum) {
		this.bilhete = bilhete;
		this.tipoItemBagagemEnum = tipoItemBagagemEnum;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public TipoItemBagagemEnum getTipoItemBagagemEnum() {
		return tipoItemBagagemEnum;
	}

	public void setTipoItemBagagemEnum(TipoItemBagagemEnum tipoItemBagagemEnum) {
		this.tipoItemBagagemEnum = tipoItemBagagemEnum;
	}
	
	public Bilhete getBilhete() {
		return bilhete;
	}
}
