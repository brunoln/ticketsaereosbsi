package br.pucpr.bsi.prog3.ticketsAereosBSI.model;

import java.util.Date;

public class Funcionario extends Pessoa {
	
	private String codigo;
	private String contaCorrente;
	private Papel papel;
	
	public Funcionario(String nome, String email, String telefone,
			Date dataNascimento, Endereco endereco) {
		super(nome, email, telefone, dataNascimento, endereco);
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getContaCorrente() {
		return contaCorrente;
	}
	public void setContaCorrente(String contaCorrente) {
		this.contaCorrente = contaCorrente;
	}
	public Papel getPapel() {
		return papel;
	}
	public void setPapel(Papel papel) {
		this.papel = papel;
	}	
}