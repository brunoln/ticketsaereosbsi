package br.pucpr.bsi.prog3.ticketsAereosBSI.model;

import java.util.List;

public class Aviao {
	
	private long id;
	private String codigo;
	private double carga;
	List<Horario> horarios;
	
	public Aviao(String codigo, double carga, List<Horario> horarios) {
		this.codigo = codigo;
		this.carga = carga;
		this.horarios = horarios;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public double getCarga() {
		return carga;
	}
	public void setCarga(double carga) {
		this.carga = carga;
	}
	public List<Horario> getHorarios() {
		return horarios;
	}
	public void setHorarios(List<Horario> horarios) {
		this.horarios = horarios;
	}
	
	@Override
	public String toString() {
		return codigo;
	}
}
