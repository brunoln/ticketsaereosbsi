package br.pucpr.bsi.prog3.ticketsAereosBSI.model;

import java.util.ArrayList;

public class Economica extends Bilhete {

	public Economica(Passageiro passageiro, SituacaoBilheteEnum situacaoBilheteEnum) {
		super(passageiro, situacaoBilheteEnum);
		bagagens = new ArrayList<ItemBagagem>(1);
	}
	
}
