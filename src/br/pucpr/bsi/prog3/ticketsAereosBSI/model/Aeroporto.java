package br.pucpr.bsi.prog3.ticketsAereosBSI.model;

import java.util.List;

public class Aeroporto {
	
	private long id;
	private String nome;
	private String codigo;
	private Endereco endereco;
	private List<Voo> voos; 

	public Aeroporto(String nome, String codigo, Endereco endereco) {
		this.nome = nome;
		this.codigo = codigo;
		this.endereco = endereco;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public List<Voo> getVoos() {
		return voos;
	}

	public void setVoos(List<Voo> voos) {
		this.voos = voos;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}