package br.pucpr.bsi.prog3.ticketsAereosBSI.model;

import java.util.List;

public class CiaAerea {

	private long id;
	private String nome;
	private List<Papel> papeis;
	private List<Voo> voos;
	
	public CiaAerea(String nome) {
		this.nome = nome;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Voo> getVoos() {
		return voos;
	}

	public void setVoos(List<Voo> voos) {
		this.voos = voos;
	}

	public List<Papel> getPapeis() {
		return papeis;
	}

	public void setPapeis(List<Papel> papeis) {
		this.papeis = papeis;
	}

	@Override
	public String toString() {
		return nome;
	}
}
