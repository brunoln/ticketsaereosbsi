package br.pucpr.bsi.prog3.ticketsAereosBSI.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Horario {
	
	private long id;
	private Date dataPartida;
	private Date dataChegada;
	private String codigo;
	private int qtdEconomica;
	private int qtdPrimeira;
	private int qtdExecutiva;
	private List<Economica> classeEconomica;
	private List<Primeira> primeiraClasse;
	private List<Executiva> classeExecutiva;
	private Aviao aviao;
	private Voo voo;
	
	public Horario(Aviao aviao, Voo voo, String codigo, Date dataPartida, 
			Date dataChegada, int qtdEconomica, int qtdExecutiva, int qtdPrimeira) {
		
		this.aviao = aviao;
		this.voo = voo;
		this.codigo = codigo;
		this.dataPartida = dataPartida;
		this.dataChegada = dataChegada;
		primeiraClasse = new ArrayList<Primeira>(qtdPrimeira);
		classeEconomica = new ArrayList<Economica>(qtdEconomica);
		classeExecutiva = new ArrayList<Executiva>(qtdExecutiva);
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Date getDataPartida() {
		return dataPartida;
	}
	public void setDataPartida(Date dataPartida) {
		this.dataPartida = dataPartida;
	}
	public Date getDataChegada() {
		return dataChegada;
	}
	public void setDataChegada(Date dataChegada) {
		this.dataChegada = dataChegada;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public int getQtdEconomica() {
		return qtdEconomica;
	}
	public void setQtdEconomica(int qtdEconomica) {
		this.qtdEconomica = qtdEconomica;
	}
	public int getQtdPrimeira() {
		return qtdPrimeira;
	}
	public void setQtdPrimeira(int qtdPrimeira) {
		this.qtdPrimeira = qtdPrimeira;
	}
	public int getQtdExecutiva() {
		return qtdExecutiva;
	}
	public void setQtdExecutiva(int qtdExecutiva) {
		this.qtdExecutiva = qtdExecutiva;
	}
	public Aviao getAviao() {
		return aviao;
	}
	public void setAviao(Aviao aviao) {
		this.aviao = aviao;
	}
	public Voo getVoo() {
		return voo;
	}

	public List<Economica> getClasseEconomica() {
		return classeEconomica;
	}

	public void setClasseEconomica(List<Economica> classeEconomica) {
		this.classeEconomica = classeEconomica;
	}

	public List<Primeira> getPrimeiraClasse() {
		return primeiraClasse;
	}

	public void setPrimeiraClasse(List<Primeira> primeiraClasse) {
		this.primeiraClasse = primeiraClasse;
	}

	public List<Executiva> getClasseExecutiva() {
		return classeExecutiva;
	}

	public void setClasseExecutiva(List<Executiva> classeExecutiva) {
		this.classeExecutiva = classeExecutiva;
	}
}
