package br.pucpr.bsi.prog3.ticketsAereosBSI.model;

import java.util.List;

public class Papel {
	
	private long id;
	private String nome;
	private String descricao;
	private CiaAerea ciaAerea;
	private List<Funcionario> funcionarios;

	public Papel(String nome, String descricao, CiaAerea ciaAerea) {
		this.nome = nome;
		this.descricao = descricao;
		this.setCiaAerea(ciaAerea);
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public List<Funcionario> getFuncionarios() {
		return funcionarios;
	}
	public void setFuncionarios(List<Funcionario> funcionarios) {
		this.funcionarios = funcionarios;
	}
	public CiaAerea getCiaAerea() {
		return ciaAerea;
	}
	public void setCiaAerea(CiaAerea ciaAerea) {
		this.ciaAerea = ciaAerea;
	}
	@Override
	public String toString() {
		return nome;
	}
}
