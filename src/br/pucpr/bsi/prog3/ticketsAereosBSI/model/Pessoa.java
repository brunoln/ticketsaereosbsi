package br.pucpr.bsi.prog3.ticketsAereosBSI.model;

import java.util.Date;

public abstract class Pessoa {

	protected long id;
	protected String nome;
	protected String email;
	protected String telefone;
	protected Date dataNascimento;
	protected Endereco endereco;
	
	public Pessoa(String nome, String email, String telefone,
			Date dataNascimento, Endereco endereco) {
		this.endereco = endereco;
		this.nome = nome;
		this.email = email;
		this.telefone = telefone;
		this.dataNascimento = dataNascimento;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public Date getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public Endereco getEndereco() {
		return endereco;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
