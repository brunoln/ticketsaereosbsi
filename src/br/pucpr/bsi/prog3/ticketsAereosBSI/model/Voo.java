package br.pucpr.bsi.prog3.ticketsAereosBSI.model;

import java.util.List;

public class Voo {
	
	private long id;
	private String nome;
	private String descricao;
	private CiaAerea cieAerea;
	private Aeroporto embarque;
	private Aeroporto desembarque;
	private Double preco;
	private List<Horario> horarios;
	
	public Voo(String nome, String descricao, 
			CiaAerea cieAerea, Aeroporto embarque, Aeroporto desembarque) {
		this.nome = nome;
		this.descricao = descricao;
		this.cieAerea = cieAerea;
		this.embarque = embarque;
		this.desembarque = desembarque;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public CiaAerea getCieAerea() {
		return cieAerea;
	}
	
	public List<Horario> getHorarios() {
		return horarios;
	}

	public void setHorarios(List<Horario> horarios) {
		this.horarios = horarios;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

	public Aeroporto getEmbarque() {
		return embarque;
	}

	public void setEmbarque(Aeroporto embarque) {
		this.embarque = embarque;
	}

	public Aeroporto getDesembarque() {
		return desembarque;
	}

	public void setDesembarque(Aeroporto desembarque) {
		this.desembarque = desembarque;
	}

	public void setCieAerea(CiaAerea cieAerea) {
		this.cieAerea = cieAerea;
	}	
	
	@Override
	public String toString() {
		return nome;
	}
}