package br.pucpr.bsi.prog3.ticketsAereosBSI.model;

import java.util.ArrayList;

public class Primeira extends Bilhete {

	public Primeira(Passageiro passageiro, SituacaoBilheteEnum situacaoBilheteEnum) {
		super(passageiro, situacaoBilheteEnum);
		bagagens = new ArrayList<ItemBagagem>(3);
	}
}
