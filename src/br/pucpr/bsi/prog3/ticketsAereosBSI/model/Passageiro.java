package br.pucpr.bsi.prog3.ticketsAereosBSI.model;

import java.util.Date;
import java.util.List;

public class Passageiro extends Pessoa {

	private String numeroCartao;
	private String documento;
	private List<Bilhete> bilhetes;
	
	public Passageiro(String nome, String email, String telefone,
			Date dataNascimento, Endereco endereco) {
		super(nome, email, telefone, dataNascimento, endereco);
	}
	
	public String getNumeroCartao() {
		return numeroCartao;
	}
	public void setNumeroCartao(String numeroCartao) {
		this.numeroCartao = numeroCartao;
	}
	public String getDocumento() {
		return documento;
	}
	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public List<Bilhete> getBilhetes() {
		return bilhetes;
	}

	public void setBilhetes(List<Bilhete> bilhetes) {
		this.bilhetes = bilhetes;
	}	
}