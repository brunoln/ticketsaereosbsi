package br.pucpr.bsi.prog3.ticketsAereosBSI.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsAereosBSI.bc.PapelBC;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.CiaAerea;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Papel;


public class PapelBCTest {
	@Test
	public void createAndRetrieveTest() {
		
		CiaAerea ciaAerea = new CiaAerea("TAM");
	
		
		Papel papel = new Papel("Piloto", "Pilota avioes",ciaAerea);
		PapelBC papelBC = PapelBC.getInstance();
		long key = papelBC.create(papel);
		assertEquals(papel, papelBC.retrieve(key));
	}
	
	@Test
	public void deletaPapelTest() {
		
		CiaAerea ciaAerea = new CiaAerea("TAM");
	
		
		Papel papel = new Papel("Piloto", "Pilota avioes",ciaAerea);
		PapelBC papelBC = PapelBC.getInstance();
		papelBC.create(papel);
		assertEquals(true, papelBC.delete(papel));
	}
	
	@Test
	public void deletaPosicaoQueNaoExisteTest() {
		
		CiaAerea ciaAerea = new CiaAerea("TAM");
		Papel papel = new Papel("Piloto", "Pilota avioes",ciaAerea);
		papel.setId(1000);
		
		PapelBC papelBC = PapelBC.getInstance();
		assertFalse(papelBC.delete(papel));
	}
	
	@Test
	public void updatePapelTest() {
		
		CiaAerea tam = new CiaAerea("TAM");
		Papel pilotoTam = new Papel("Piloto", "Pilota avioes",tam);
		
		
		PapelBC papelBC = PapelBC.getInstance();
		papelBC.create(pilotoTam);
		
		pilotoTam.setNome("Nome Alterado");
		papelBC.update(pilotoTam);
		
		
		assertEquals("Nome Alterado", papelBC.retrieve(pilotoTam.getId()).getNome());
	}
	
	@Test
	public void updateEmPosicaoQueNaoExisteTest() {
		
		CiaAerea gol = new CiaAerea("GOL");
		Papel pilotoGol = new Papel("Piloto", "Pilota avioes",gol);
		pilotoGol.setId(1000);
		
		PapelBC papelBC = PapelBC.getInstance();
		assertFalse(papelBC.update(pilotoGol));
	}

}
