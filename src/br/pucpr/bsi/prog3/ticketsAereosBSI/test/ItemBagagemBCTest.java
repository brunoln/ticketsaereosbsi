package br.pucpr.bsi.prog3.ticketsAereosBSI.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.Date;

import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsAereosBSI.bc.ItemBagagemBC;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Bilhete;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Economica;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.ItemBagagem;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Passageiro;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.SituacaoBilheteEnum;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.TipoItemBagagemEnum;

public class ItemBagagemBCTest {

	@Test
	public void createAndRetrieveTest() {
		Endereco endereco = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");
		
		Passageiro passageiro = new Passageiro("Bruno Nascimento", "b@email.com",
				"5555-5555", new Date(), endereco);
		
		Bilhete bilhete = new Economica(passageiro, SituacaoBilheteEnum.DISPONIVEL);
		ItemBagagem itemBagagem = new ItemBagagem(bilhete, TipoItemBagagemEnum.MALA);
		
		ItemBagagemBC itemBagagemBC = ItemBagagemBC.getInstance();
		long key = itemBagagemBC.create(itemBagagem);
		assertEquals(itemBagagem, itemBagagemBC.retrieve(key));
	}
	
	@Test
	public void deletaItemBagagemTest() {
		Endereco endereco = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");
		
		Passageiro passageiro = new Passageiro("Bruno Nascimento", "b@email.com",
				"5555-5555", new Date(), endereco);
		
		Bilhete bilhete = new Economica(passageiro, SituacaoBilheteEnum.DISPONIVEL);
		ItemBagagem itemBagagem = new ItemBagagem(bilhete, TipoItemBagagemEnum.MALA);
		
		ItemBagagemBC itemBagagemBC = ItemBagagemBC.getInstance();
		itemBagagemBC.create(itemBagagem);
		assertEquals(true, itemBagagemBC.delete(itemBagagem));
	}
	
	@Test
	public void deletaPosicaoQueNaoExisteTest() {
		Endereco endereco = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");
		
		Passageiro passageiro = new Passageiro("Bruno Nascimento", "b@email.com",
				"5555-5555", new Date(), endereco);
		
		Bilhete bilhete = new Economica(passageiro, SituacaoBilheteEnum.DISPONIVEL);
		ItemBagagem itemBagagem = new ItemBagagem(bilhete, TipoItemBagagemEnum.MALA);
		itemBagagem.setId(1000);
		
		
		ItemBagagemBC itemBagagemBC = ItemBagagemBC.getInstance();
		assertFalse(itemBagagemBC.delete(itemBagagem));
	}
	
	@Test
	public void updateItemBagagemTest() {
		Endereco enderecoBruno = new Endereco("Avenida Washington Luis", 1, "casa", "Jardim Aeroporto",
				"Sao Paulo", "Sao Paulo", "Brasil");
			
		Endereco enderecoCaroline = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");
		
		Passageiro passageiroBruno = new Passageiro("Bruno Nascimento", "b@email.com",
				"5555-5555", new Date(), enderecoBruno);
		Passageiro passageiroCaroline = new Passageiro("Caroline Nardino", "cn@email.com",
				"5555-5555", new Date(), enderecoCaroline);
		
		Bilhete bilheteBruno = new Economica(passageiroBruno, SituacaoBilheteEnum.DISPONIVEL);
		Bilhete bilheteCaroline = new Economica(passageiroCaroline, SituacaoBilheteEnum.DISPONIVEL);
		
		ItemBagagem itemBagagemBruno = new ItemBagagem(bilheteBruno, TipoItemBagagemEnum.MALA);
		ItemBagagem itemBagagemCaroline = new ItemBagagem(bilheteCaroline, TipoItemBagagemEnum.MAO);
		
		ItemBagagemBC itemBagagemBC = ItemBagagemBC.getInstance();
		long key = itemBagagemBC.create(itemBagagemBruno);
		
		itemBagagemBC.update(key, itemBagagemCaroline);
		assertEquals(itemBagagemCaroline, itemBagagemBC.retrieve(key));
	}
	
	@Test
	public void updateEmPosicaoQueNaoExisteTest() {
		Endereco endereco = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");
		
		Passageiro passageiro = new Passageiro("Bruno Nascimento", "b@email.com",
				"5555-5555", new Date(), endereco);
		
		Bilhete bilhete = new Economica(passageiro, SituacaoBilheteEnum.DISPONIVEL);
		ItemBagagem itemBagagem = new ItemBagagem(bilhete, TipoItemBagagemEnum.MALA);
		itemBagagem.setId(1000);
		
		ItemBagagemBC itemBagagemBC = ItemBagagemBC.getInstance();
		assertFalse(itemBagagemBC.update(1000000, itemBagagem));
	}
}