package br.pucpr.bsi.prog3.ticketsAereosBSI.test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsAereosBSI.bc.VooBC;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Aeroporto;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.CiaAerea;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Voo;

public class VooBCTest {

	@Test
	public void createAndRetrieveTest() {
		Endereco endereco = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");
		
		Aeroporto aeroportoCongonhas = new Aeroporto("Congonhas", "C1", endereco);
		Aeroporto aeroportoGuarulhos = new Aeroporto("Guarulhos", "C1", endereco);
		CiaAerea ciaAerea = new CiaAerea("TAM");

		Voo voo = new Voo("Curitiba", "Voo para curitiba", ciaAerea, aeroportoCongonhas, aeroportoGuarulhos);
		
		VooBC vooBC = VooBC.getInstance();
		long key = vooBC.create(voo);
		assertEquals(voo, vooBC.retrieve(key));
	}
	
	@Test
	public void deleteVooTest() {
		Endereco endereco = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");
		
		Aeroporto aeroportoCongonhas = new Aeroporto("Congonhas", "C1", endereco);
		Aeroporto aeroportoGuarulhos = new Aeroporto("Guarulhos", "C1", endereco);
		CiaAerea ciaAerea = new CiaAerea("TAM");
		Voo voo = new Voo("Curitiba", "Voo para curitiba", ciaAerea, aeroportoCongonhas, aeroportoGuarulhos);
		
		VooBC vooBC = VooBC.getInstance();
		vooBC.create(voo);		
		assertEquals(true, vooBC.delete(voo));
	}
	
	@Test
	public void deletaPosicaoQueNaoExisteTest() {
		Endereco endereco = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");
		
		Aeroporto aeroportoCongonhas = new Aeroporto("Congonhas", "C1", endereco);
		Aeroporto aeroportoGuarulhos = new Aeroporto("Guarulhos", "C1", endereco);
		CiaAerea ciaAerea = new CiaAerea("TAM");
		Voo voo = new Voo("Curitiba", "Voo para curitiba", ciaAerea, aeroportoCongonhas, aeroportoGuarulhos);
		voo.setId(1000);
		VooBC vooBC = VooBC.getInstance();
		
		assertFalse(vooBC.delete(voo));
	}
	
	@Test
	public void updateVooTest() {
		Endereco endereco = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");
		
		Aeroporto aeroportoCongonhas = new Aeroporto("Congonhas", "C1", endereco);
		Aeroporto aeroportoGuarulhos = new Aeroporto("Guarulhos", "C1", endereco);
		CiaAerea ciaAerea = new CiaAerea("TAM");

		Voo voo = new Voo("Curitiba", "Voo para curitiba", ciaAerea, aeroportoCongonhas, aeroportoGuarulhos);

		VooBC vooBC = VooBC.getInstance();
		vooBC.create(voo);
		voo.setDescricao("Outro Voo qualquer");
		vooBC.update(voo);
		assertEquals("Outro Voo qualquer", vooBC.retrieve(voo.getId()).getDescricao());
	}
	
	@Test
	public void updateEmPosicaoQueNaoExisteTest() {
		Endereco endereco = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");
		
		Aeroporto aeroportoCongonhas = new Aeroporto("Congonhas", "C1", endereco);
		Aeroporto aeroportoGuarulhos = new Aeroporto("Guarulhos", "C1", endereco);
		CiaAerea ciaAerea = new CiaAerea("TAM");

		Voo voo = new Voo("Curitiba", "Voo para curitiba", ciaAerea, aeroportoCongonhas, aeroportoGuarulhos);
		voo.setId(1000);
		
		
		VooBC vooBC = VooBC.getInstance();
		assertFalse(vooBC.update(voo));
	}
}
