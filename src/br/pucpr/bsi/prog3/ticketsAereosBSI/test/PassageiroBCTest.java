package br.pucpr.bsi.prog3.ticketsAereosBSI.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.Date;

import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsAereosBSI.bc.PassageiroBC;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Passageiro;

public class PassageiroBCTest {
	
	@Test
	public void createAndRetrieveTest() {
		Endereco endereco = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");
		
		Passageiro passageiro = new Passageiro("Bruno Nascimento", "b@email.com",
				"5555-5555", new Date(), endereco);
		
		PassageiroBC passageiroBC = PassageiroBC.getInstance();
		long key = passageiroBC.create(passageiro);
		assertEquals(passageiro, passageiroBC.retrieve(key));
	}
	
	@Test
	public void deletaPassageiroTest() {
		Endereco endereco = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");
		
		Passageiro passageiro = new Passageiro("Bruno Nascimento", "b@email.com",
				"5555-5555", new Date(), endereco);
		
		PassageiroBC passageiroBC = PassageiroBC.getInstance();
		passageiroBC.create(passageiro);
		assertEquals(true, passageiroBC.delete(passageiro));
	}
	
	@Test
	public void deletaPosicaoQueNaoExisteTest() {
		Endereco endereco = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");
		Passageiro passageiro = new Passageiro("Bruno Nascimento", "b@email.com",
				"5555-5555", new Date(), endereco);
		passageiro.setId(1000);
		
		PassageiroBC passageiroBC = PassageiroBC.getInstance();
		assertFalse(passageiroBC.delete(passageiro));
	}
	
	@Test
	public void updatePassageiroTest() {
		Endereco enderecoBruno = new Endereco("Avenida Washington Luis", 1, "casa", "Jardim Aeroporto",
				"Sao Paulo", "Sao Paulo", "Brasil");
		
		Passageiro passageiroBruno = new Passageiro("Bruno Nascimento", "b@email.com",
				"5555-5555", new Date(), enderecoBruno);
		
		PassageiroBC passageiroBC = PassageiroBC.getInstance();
		passageiroBC.create(passageiroBruno);
		
		passageiroBruno.setEmail("br@email.com");
		passageiroBC.update(passageiroBruno);
		
		assertEquals("br@email.com", passageiroBC.retrieve(passageiroBruno.getId()).getEmail());
	}
	
	@Test
	public void updateEmPosicaoQueNaoExisteTest() {
		Endereco enderecoCaroline = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");
		
		Passageiro passageiraCaroline = new Passageiro("Caroline Nardino", "cn@email.com",
				"5555-5555", new Date(), enderecoCaroline);
		passageiraCaroline.setId(1000);
		
		PassageiroBC passageiroBC = PassageiroBC.getInstance();
		assertFalse(passageiroBC.update(passageiraCaroline));
	}

}
