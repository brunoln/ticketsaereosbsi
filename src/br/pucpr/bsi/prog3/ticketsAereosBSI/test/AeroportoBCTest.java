package br.pucpr.bsi.prog3.ticketsAereosBSI.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsAereosBSI.bc.AeroportoBC;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Aeroporto;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Endereco;

public class AeroportoBCTest {

	@Test
	public void createAndRetrieveTest() {
		Endereco endereco = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");

		Aeroporto aeroporto = new Aeroporto("Congonhas", "C1", endereco);
		
		AeroportoBC gerenciador = AeroportoBC.getInstance();
		long key = gerenciador.create(aeroporto);
		assertEquals(aeroporto, gerenciador.retrieve(key));
	}
	
	@Test
	public void deletaAeroportoTest() {
		Endereco endereco = new Endereco("Rua alferes Poli", 464, "casa", "Reboucas",
				"Curitiba", "Parana", "Brasil");
		
		Aeroporto aeroporto = new Aeroporto("Congonhas", "C1", endereco);
		
		AeroportoBC gerenciador = AeroportoBC.getInstance();
		gerenciador.create(aeroporto);
		assertEquals(true, gerenciador.delete(aeroporto));
	}
	
	@Test
	public void deletaPosicaoQueNaoExisteTest() {
		AeroportoBC aeroportoBC = AeroportoBC.getInstance();
		assertFalse(aeroportoBC.delete(new Aeroporto("1231", "21321", null)));
	}
	
	@Test
	public void updateAeroportoTest() {
		Endereco enderecoCongonhas = new Endereco("Avenida Washington Luis", 1, "casa", "Jardim Aeroporto",
				"Sao Paulo", "Sao Paulo", "Brasil");
			
		Aeroporto congonhas = new Aeroporto("Congonhas", "C1", enderecoCongonhas);
		AeroportoBC aeroportoBC = AeroportoBC.getInstance();
		
		aeroportoBC.create(congonhas);
		congonhas.setCodigo("C2");
		assertEquals("C2", aeroportoBC.retrieve(congonhas.getId()).getCodigo());
	}
	
	@Test
	public void updateEmPosicaoQueNaoExisteTest() {
		Endereco enderecoCongonhas = new Endereco("Avenida Washington Luis", 1, "casa", "Jardim Aeroporto",
				"Sao Paulo", "Sao Paulo", "Brasil");

		Aeroporto congonhas = new Aeroporto("Congonhas", "C1", enderecoCongonhas);
		congonhas.setId(1000);
		AeroportoBC aeroportoBC = AeroportoBC.getInstance();
		
		assertFalse(aeroportoBC.update(congonhas));
	}

}

