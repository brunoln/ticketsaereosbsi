package br.pucpr.bsi.prog3.ticketsAereosBSI.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.Date;

import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsAereosBSI.bc.FuncionarioBC;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Funcionario;

public class FuncionarioBCTest {

	@Test
	public void createAndRetrieveTest() {
		Endereco endereco = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");
		
		Funcionario funcionario = new Funcionario( "Bruno Nascimento", "b@email.com",
				"5555-5555", new Date(), endereco);
		
		FuncionarioBC funcionarioBC = FuncionarioBC.getInstance();
		long key = funcionarioBC.create(funcionario);
		assertEquals(funcionario, funcionarioBC.retrieve(key));
	}
	
	@Test
	public void deletaFuncionarioTest() {
		Endereco endereco = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");
		
		Funcionario funcionario = new Funcionario("Bruno Nascimento", "b@email.com",
				"5555-5555", new Date(), endereco);
		
		FuncionarioBC funcionarioBC = FuncionarioBC.getInstance();
		funcionarioBC.create(funcionario);
		assertEquals(true, funcionarioBC.delete(funcionario));
	}
	
	@Test
	public void deletaPosicaoQueNaoExisteTest() {
		
		Funcionario funcionario = new Funcionario("Bruno Nascimento", "b@email.com",
				"5555-5555", new Date(), null);
		funcionario.setId(1000);
		
		FuncionarioBC funcionarioBC = FuncionarioBC.getInstance();
		assertFalse(funcionarioBC.delete(funcionario));
	}
	
	@Test
	public void updateFuncionarioTest() {
		Endereco enderecoCaroline = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");

		Funcionario funcionariaCaroline = new Funcionario("Caroline Nardino", "cn@email.com",
				"5555-5555", new Date(), enderecoCaroline);
		
		FuncionarioBC funcionarioBC = FuncionarioBC.getInstance();
		funcionarioBC.create(funcionariaCaroline);
		
		funcionariaCaroline.setEmail("cn2@email.com");
		
		funcionarioBC.update(funcionariaCaroline);
		
		assertEquals("cn2@email.com", funcionarioBC.retrieve(
				funcionariaCaroline.getId()).getEmail());
	}
	
	@Test
	public void updateEmPosicaoQueNaoExisteTest() {
		Endereco enderecoBruno = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");
		
		Funcionario funcionarioBruno = new Funcionario("Bruno Nascimento", "b@email.com",
				"5555-5555", new Date(), enderecoBruno);
		
		funcionarioBruno.setId(1000);
	
		FuncionarioBC funcionarioBC = FuncionarioBC.getInstance();
		assertFalse(funcionarioBC.update(funcionarioBruno));
	}
}
