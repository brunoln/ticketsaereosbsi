package br.pucpr.bsi.prog3.ticketsAereosBSI.test;


public class HorarioBCTest {
/*	@Test
	public void createAndRetrieveTest() {
		Endereco endereco = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");
		
		Aeroporto aeroportoCongonhas = new Aeroporto("Congonhas", "C1", endereco);
		Aeroporto aeroportoGuarulhos = new Aeroporto("Guarulhos", "C1", endereco);
		CiaAerea ciaAerea = new CiaAerea("TAM");

		Voo voo = new Voo("Curitiba", "Voo para curitiba", ciaAerea, aeroportoCongonhas, aeroportoGuarulhos);
		
		Horario horario = new Horario(10, 10, 10, voo);
		
		HorarioBC horarioBC = HorarioBC.getInstance();
		long key = horarioBC.create(horario);
		assertEquals(horario, horarioBC.retrieve(key));
	}
	
	@Test
	public void deleteHorarioTest() {
		Endereco endereco = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");
		
		Aeroporto aeroportoCongonhas = new Aeroporto("Congonhas", "C1", endereco);
		Aeroporto aeroportoGuarulhos = new Aeroporto("Guarulhos", "C1", endereco);
		CiaAerea ciaAerea = new CiaAerea("TAM");

		Voo voo = new Voo("Curitiba", "Voo para curitiba", ciaAerea, aeroportoCongonhas, aeroportoGuarulhos);
		
		Horario horario = new Horario(10, 10, 10, voo);
		
		HorarioBC horarioBC = HorarioBC.getInstance();
		horarioBC.create(horario);		
		assertEquals(true, horarioBC.delete(horario));
	}
	
	@Test
	public void deletaPosicaoQueNaoExisteTest() {
		Endereco endereco = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");
		
		Aeroporto aeroportoCongonhas = new Aeroporto("Congonhas", "C1", endereco);
		Aeroporto aeroportoGuarulhos = new Aeroporto("Guarulhos", "C1", endereco);
		CiaAerea ciaAerea = new CiaAerea("TAM");

		Voo voo = new Voo("Curitiba", "Voo para curitiba", ciaAerea, aeroportoCongonhas, aeroportoGuarulhos);
		
		Horario horario = new Horario(10, 10, 10, voo);
		horario.setId(1000);
		
		
		HorarioBC horarioBC = HorarioBC.getInstance();
		assertFalse(horarioBC.delete(horario));
	}
	
	@Test
	public void updateHorarioTest() {
		Endereco endereco = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");
		
		Aeroporto aeroportoCongonhas = new Aeroporto("Congonhas", "C1", endereco);
		Aeroporto aeroportoGuarulhos = new Aeroporto("Guarulhos", "C1", endereco);
		CiaAerea ciaAerea = new CiaAerea("TAM");

		Voo voo = new Voo("Curitiba", "Voo para curitiba", ciaAerea, aeroportoCongonhas, aeroportoGuarulhos);
		Voo voo2 = new Voo("Test", "VTeste", ciaAerea, aeroportoCongonhas, aeroportoGuarulhos);
		
		Horario horario = new Horario(10, 10, 10, voo);
		Horario horario2 = new Horario(10, 10, 10, voo2);
		
		HorarioBC horarioBC = HorarioBC.getInstance();
		long key = horarioBC.create(horario);		
		horarioBC.update(horario2);
		assertEquals(horario2, horarioBC.retrieve(key));
	}
	
	@Test
	public void updateEmPosicaoQueNaoExisteTest() {
		Endereco endereco = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");
		
		Aeroporto aeroportoCongonhas = new Aeroporto("Congonhas", "C1", endereco);
		Aeroporto aeroportoGuarulhos = new Aeroporto("Guarulhos", "C1", endereco);
		CiaAerea ciaAerea = new CiaAerea("TAM");

		Voo voo = new Voo("Curitiba", "Voo para curitiba", ciaAerea, aeroportoCongonhas, aeroportoGuarulhos);
		Horario horario = new Horario(10, 10, 10, voo);
		horario.setId(1000);
		
		HorarioBC horarioBC = HorarioBC.getInstance();
		assertFalse(horarioBC.update(horario));
	}
*/
}
