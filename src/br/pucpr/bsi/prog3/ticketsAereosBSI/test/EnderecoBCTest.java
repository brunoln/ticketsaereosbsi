package br.pucpr.bsi.prog3.ticketsAereosBSI.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsAereosBSI.bc.EnderecoBC;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Endereco;

public class EnderecoBCTest {

	@Test
	public void createAndRetrieveTest() {
		Endereco endereco = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");
		
		EnderecoBC enderecoBC = EnderecoBC.getInstance();
		long key = enderecoBC.create(endereco);
		assertEquals(endereco, enderecoBC.retrieve(key));
	}
	
	@Test
	public void deletaEnderecoTest() {
		Endereco endereco = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");
		
		EnderecoBC enderecoBC = EnderecoBC.getInstance();
		enderecoBC.create(endereco);
		assertEquals(true, enderecoBC.delete(endereco));
	}
	
	@Test
	public void deletaPosicaoQueNaoExisteTest() {
		Endereco endereco = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");
		endereco.setId(1000);
		
		EnderecoBC enderecoBC = EnderecoBC.getInstance();
		assertFalse(enderecoBC.delete(endereco));
	}
	
	@Test
	public void updateEnderecoTest() {
		Endereco enderecoBruno = new Endereco("Avenida Washington Luis", 1, "casa", "Jardim Aeroporto",
				"Sao Paulo", "Sao Paulo", "Brasil");

		EnderecoBC enderecoBC = EnderecoBC.getInstance();
		enderecoBC.create(enderecoBruno);
		
		enderecoBruno.setRua("Outra Avenida qualquer");
		
		enderecoBC.update(enderecoBruno);
		
		assertEquals("Outra Avenida qualquer", 
				enderecoBC.retrieve(enderecoBruno.getId()).getRua());
	}
	
	@Test
	public void updateEmPosicaoQueNaoExisteTest() {
		Endereco enderecoBruno = new Endereco("Avenida Washington Luis", 1, "casa", "Jardim Aeroporto",
				"Sao Paulo", "Sao Paulo", "Brasil");
		
		enderecoBruno.setId(1000);
		
		EnderecoBC enderecoBC = EnderecoBC.getInstance();
		assertFalse(enderecoBC.update(enderecoBruno));
	}

}
