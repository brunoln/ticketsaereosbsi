package br.pucpr.bsi.prog3.ticketsAereosBSI.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.Date;

import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsAereosBSI.bc.BilheteBC;
import br.pucpr.bsi.prog3.ticketsAereosBSI.bc.PassageiroBC;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Bilhete;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Economica;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.Passageiro;
import br.pucpr.bsi.prog3.ticketsAereosBSI.model.SituacaoBilheteEnum;

public class BilheteBCTest {

	@Test
	public void createAndRetrieveTest() {
		Endereco endereco = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");
		
		Passageiro passageiro = new Passageiro("Bruno Nascimento", "b@email.com",
				"5555-5555", new Date(), endereco);
		
		Bilhete bilhete = new Economica(passageiro, SituacaoBilheteEnum.DISPONIVEL);
		BilheteBC bilheteBC = BilheteBC.getInstance();
		long key = bilheteBC.create(bilhete);
		assertEquals(bilhete, bilheteBC.retrieve(key));
	}
	
	@Test
	public void deletaBilheteTest() {
		Endereco endereco = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");
		
		Passageiro passageiro = new Passageiro("Bruno Nascimento", "b@email.com",
				"5555-5555", new Date(), endereco);
		
		Bilhete bilhete = new Economica(passageiro, SituacaoBilheteEnum.DISPONIVEL);
		BilheteBC bilheteBC = BilheteBC.getInstance();
		bilheteBC.create(bilhete);
		assertEquals(true, bilheteBC.delete(bilhete));
	}
	
	@Test
	public void deletaPosicaoQueNaoExisteTest() {
		Bilhete bilhete = new Economica(null, SituacaoBilheteEnum.DISPONIVEL);
		bilhete.setId(1000);
		
		BilheteBC bilheteBC = BilheteBC.getInstance();
		assertFalse(bilheteBC.delete(bilhete));
	}
	
	@Test
	public void updateBilheteTest() {
		Endereco enderecoBruno = new Endereco("Avenida Washington Luis", 1, "casa", "Jardim Aeroporto",
				"Sao Paulo", "Sao Paulo", "Brasil");
		Passageiro passageiroBruno = new Passageiro("Bruno Nascimento", "b@email.com",
				"5555-5555", new Date(), enderecoBruno);
	
		Bilhete bilheteBruno = new Economica(passageiroBruno, SituacaoBilheteEnum.DISPONIVEL);
		
		BilheteBC bilheteBC = BilheteBC.getInstance();
		bilheteBC.create(bilheteBruno);
		bilheteBruno.setNumero(26);
		
		bilheteBC.update(bilheteBruno);
		assertEquals(26, bilheteBC.retrieve(bilheteBruno.getId()).getNumero());
	}
	
	@Test
	public void updateEmPosicaoQueNaoExisteTest() {
		Endereco enderecoCaroline = new Endereco("Rua alferes Poli", 464, "casa", "Rebouças",
				"Curitiba", "Paraná", "Brasil");
		
		Passageiro passageiroCaroline = new Passageiro( "Caroline Nardino", "cn@email.com",
				"5555-5555", new Date(), enderecoCaroline);
		passageiroCaroline.setId(1000);
		
		PassageiroBC passageiroBC = PassageiroBC.getInstance();
		assertFalse(passageiroBC.update(passageiroCaroline));
	}
}
